import { View, Text } from 'react-native';
import React, { useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import MainRoute from './src/Routes/MainRoute';
import { Provider as PaperProvider } from 'react-native-paper';
import OneSignal from 'react-native-onesignal';
import AsyncStorage from '@react-native-async-storage/async-storage';


const APP_ID = '25d7dd24-15a9-450a-9fc6-870e68e61941'
const SENDER_ID = '1088833333526'

const App = () => {

  useEffect(() => {
    OneSignal.setLogLevel(6, 0);
    OneSignal.setAppId(APP_ID);

    OneSignal.getDeviceState().then(async (res) => {
      console.log(res)
      await AsyncStorage.setItem('pushObj', JSON.stringify(res))
    })

    OneSignal.setNotificationWillShowInForegroundHandler(notificationReceivedEvent => {
      console.log("OneSignal: notification will show in foreground:", notificationReceivedEvent);
      let notification = notificationReceivedEvent.getNotification();
      console.log("notification: ", notification);
      const data = notification.additionalData
      console.log("additionalData: ", data);
      // Complete with null means don't show a notification.
      notificationReceivedEvent.complete(notification);
    });

    OneSignal.setNotificationOpenedHandler(notification => {
      console.log("OneSignal: notification opened:", notification);
    });

  }, [OneSignal])

  return (
    <PaperProvider>
      <NavigationContainer>
        <MainRoute />
      </NavigationContainer>
    </PaperProvider>
  );
};

export default App;
