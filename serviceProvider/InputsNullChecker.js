const checkForEmptyKeys = function(dataObj) {
  let emptyInputs = [];
  let errorString = "Please enter ";
  for (var key in dataObj) {
    dataObj[key] = dataObj[key]?.trim()
    if (dataObj[key] == "" || dataObj[key] == undefined) {
      errorString += key.toUpperCase() + ", ";
      emptyInputs.push(key.toUpperCase());
    }
  }
  errorString = errorString.replace(/,(?!.*?,)/, "");
  return { anyEmptyInputs: emptyInputs, errorString };
};

const checkNumber = (valType) =>{
  let isnum = /^\d+$/.test(valType);
  return isnum;
};

const checkEmail = (emailval) =>{
  const reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ ;
  let isvalid = reg.test(emailval);
  return isvalid;
}

export { checkForEmptyKeys, checkNumber,checkEmail};
