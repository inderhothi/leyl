import { View, Text } from 'react-native';
import React from 'react';
import LinearGradient from "react-native-linear-gradient";
import IonIcon from 'react-native-vector-icons/Ionicons';

const SuccessPage = (props) => {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <LinearGradient
                colors={['#E98607', '#9013D1']} style={{ flex: 1 }}>
                    <IonIcon name='checkmark-circle' size={50} color={'#fff'}  />
            </LinearGradient>
        </View>
    );
};

export default SuccessPage;
