import { View, Text, Image, TouchableOpacity, StyleSheet, TextInput, PermissionsAndroid } from 'react-native';
import React, { useEffect, useState } from 'react';
import LinearGradient from "react-native-linear-gradient";
// import Contacts from 'react-native-contacts';
import SuccessPage from '../../Styles/SuccessPage';
import IonIcon from 'react-native-vector-icons/Ionicons';
import { selectContactPhone } from 'react-native-select-contact';
import Toaster from '../../Styles/Toaster';
import Spinner from '../../Styles/Spinner';
import { Post } from '../../../Helpers/Service';





const SendMoney = (props) => {
    const [loading, setLoading] = useState(false);
    const [showThanks, setShowThanks] = useState(false);
    const [phoneBookDetail, setPhoneBookDetail] = useState({});
    // const [dollar, setDollar] = useState('$');
    const [pay, setPay] = useState({
        amount: '',
        name: '',
        message: ''
    })

    useEffect(() => {
        let data = props?.route?.params?.qrValue;
        if (data != undefined) {
            const qdata = (JSON.parse(data));
            setPay({ ...pay, name: JSON.parse(qdata.data) })
        }

    }, [])

    const getPhoneNumber = async () => {
        const permition = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
            {
                'title': 'Contacts',
                'message': 'This app would like to view your contacts.',
                'buttonPositive': 'Please accept bare mortal'
            }
        )
        if (permition == 'granted') {
            selectContactPhone().then(selection => {
                if (!selection) {
                    return null;
                }
                let { contact, selectedPhone } = selection;
                // let x = selectedPhone.number.replace(/\D/g, '').match(/(\d{1})(\d{0,3})(\d{0,3})(\d{0,2})(\d{0,2})/);
                let x = selectedPhone.number.replace(/\D/g, '');
                if (x.length == 12) {
                    x = x.substring(2)
                }
                setPay({ ...pay, name: x })
                console.log(x);
                setPhoneBookDetail(selection)
            })
        }

    }





    const sendMoney = () => {
        if (pay.amount.length == 0) {
            Toaster('please enter amount then proceed')
            return;
        }
        if (pay.message.length == 0) {
            Toaster('please add a message then proceed')
            return;
        }
        setLoading(true);
        const data = { phone: pay?.name || '' }
        Post('get-user', data).then((res) => {
            if (res.user != undefined) {
                const d = {
                    amount: pay?.amount || '',
                    user_id: res.user.id,
                    message: pay?.message || ''
                }
                Post('transfer-money', d).then((res) => {
                    if (res.status == 200) {
                        setShowThanks(true);
                    }
                    Toaster(res.message)
                    setLoading(false);
                }, err => {
                    Toaster(res.message)
                    setLoading(false);
                    console.log(err)
                })
            } else {
                Toaster(res.message)
            }
            setLoading(false);
        }, err => {
            Toaster(res.message)
            setLoading(false);
            console.log(err)
        })
    }
    return (
        <>
            <Spinner color={'#fff'} visible={loading} />
            {!showThanks ? <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#000000' }}>
                <Text style={[styles.contactLabel, { width: '100%', textAlign: 'left', padding: 20 }]}>Amount</Text>
                <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={styles.money}>CHF</Text>
                    <TextInput keyboardType='numeric' value={pay.amount} onChangeText={(text) => { setPay({ ...pay, amount: text }) }} style={styles.money} />
                </View>
                {/* <TextInput value={`${dollar} ${pay.amount}`} onChangeText={(text) => { setPay({ ...pay, amount: text }); setDollar('') }} style={styles.money} /> */}
                <View style={{ width: '100%', padding: 20, marginTop: 20 }}>
                    <View style={[styles.inputView, { position: 'relative' }]}>
                        <Text style={styles.contactLabel}>Contact</Text>
                        <TextInput value={pay.name} onChangeText={(text) => { setPay({ ...pay, name: text }) }} placeholder='Enter Name or Mobile Number' placeholderTextColor={'grey'} style={{ color: '#fff' }} />
                        <TouchableOpacity style={{ position: 'absolute', right: 0, height: 40, width: 30, top: 30 }} onPress={() => { getPhoneNumber() }} >
                            <Image source={require('../../Assets/Images/contact-book.png')} style={{ height: 20, width: 18 }} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.inputView}>
                        <Text style={styles.contactLabel}>Message</Text>
                        <TextInput value={pay.message} onChangeText={(text) => { setPay({ ...pay, message: text }) }} placeholder='Add a Message' multiline={true} placeholderTextColor={'grey'} style={{ color: '#fff' }} />
                    </View>
                </View>
                <TouchableOpacity style={{ width: '90%', height: 55, marginTop: 40, borderRadius: 10 }}
                    onPress={() => { sendMoney() }}>

                    <LinearGradient
                        colors={['#E98607', '#9013D1']} style={styles.loginLiner}>
                        <Text style={{ color: '#000000', fontSize: 18, fontFamily: 'Montserrat-Light', fontWeight: '700', marginLeft: 10 }}>Send Money</Text>
                    </LinearGradient>
                </TouchableOpacity>

            </View>
                :
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#000000', padding: 30, position: 'relative' }}>
                    {/* <LinearGradient
                        colors={['#E98607', '#9013D1']} style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}> */}
                    <Image source={require('../../Assets/Images/checked.png')} style={{ height: 60, width: 60 }} />
                    {/* <IonIcon name='checkmark-circle' size={100} color={'#fff'} /> */}
                    <Text style={{ color: '#fff', fontSize: 22, fontFamily: 'Montserrat-Light', fontWeight: '700', marginLeft: 10, marginTop: 10 }}>Success</Text>
                    <Text style={{ color: '#fff', fontSize: 14, fontFamily: 'Montserrat-Light', fontWeight: '700', marginLeft: 10, marginVertical: 10 }}>Money was sent to {phoneBookDetail?.contact?.name || pay?.name} Successfully</Text>
                    <Text style={{ color: '#fff', fontSize: 30, fontFamily: 'Montserrat-Medium' }}>${pay.amount}</Text>
                    {/* </LinearGradient> */}
                    <TouchableOpacity style={{ position: 'absolute', bottom: 40, width: '100%', height: 55, marginTop: 40, borderRadius: 10 }}
                        onPress={() => { props.navigation.navigate('Home') }}>

                        <LinearGradient
                            colors={['#E98607', '#9013D1']} style={styles.loginLiner}>
                            <Text style={{ color: '#000000', fontSize: 18, fontFamily: 'Montserrat-SemiBold', marginLeft: 10 }}>Okay</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
            }
        </>
    );
};

const styles = StyleSheet.create({
    loginLiner: {
        width: '100%',
        height: 55,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        flexDirection: 'row'
    },
    money: {
        fontSize: 40,
        color: '#fff',
        fontFamily: 'Montserrat-Regular',
        justifyContent: 'center',
        alignItems: 'center'
    },
    contactLabel: {
        fontSize: 12,
        color: 'grey',
        fontFamily: 'Montserrat-Bold',
        color: '#fff',

    },
    inputView: {
        borderBottomColor: '#1D1D1D',
        borderBottomWidth: 1,
        marginVertical: 10,
        color: '#fff',
    }
});
export default SendMoney;
