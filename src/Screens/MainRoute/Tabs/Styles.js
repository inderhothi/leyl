import { StyleSheet } from "react-native"

const Styles = StyleSheet.create({
    shadowProp: {
        shadowColor: "lightgrey",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        height: 40,
        elevation: 12,
        marginTop: 20,
        marginHorizontal: 20,
        borderRadius: 10,
        fontFamily: 'Montserrat-Bold',
        fontSize: 12
    },

    cardShadow: {
        shadowColor: "lightgrey",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 12,
        borderRadius: 5,
        backgroundColor: '#fff',
        height: 110,
        width: 110,
        justifyContent: 'center',
        alignItems: 'center',
    },

    cardShadow2: {
        shadowColor: "lightgrey",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 12,
        borderRadius: 5,
        backgroundColor: '#fff',
        height: 80,
        width: '100%',
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'row'
    },

    card2: {
        // flex:1,
        justifyContent: 'center',
        alignItems: 'center',

    },
    card: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',

    },

    sendQr: {
        flex: 1,
        backgroundColor: 'black',
        borderRadius: 10,
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 5,
        flexDirection: 'row'
    }
});

export default Styles;
