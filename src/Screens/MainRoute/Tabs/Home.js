import { View, Text, ScrollView, SafeAreaView, Image, Dimensions, TouchableOpacity } from 'react-native';
import React, { useState, useEffect } from 'react';
import { Searchbar, Card, Avatar } from 'react-native-paper';
import Styles from './Styles';
import { Post } from '../../../../Helpers/Service';
import Spinner from '../../../Styles/Spinner'
import moment from 'moment';
import AsyncStorage from '@react-native-async-storage/async-storage';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Carousel, { Pagination } from 'react-native-snap-carousel';

const Home = (props) => {


  const [searchQuery, setSearchQuery] = React.useState('');
  const [transList, setTransList] = useState([]);
  const [loading, setLoading] = useState(false);
  const [userDetail, setUserDetail] = useState({});

  useEffect(() => {
    getTransactionList();

    (async () => {
      const usedetail = await AsyncStorage.getItem('userDetail');
      if (usedetail) {
        setUserDetail(JSON.parse(usedetail))
      }
    })();
    addmoney();

    const willFocusSubscription = props.navigation.addListener('focus', () => {
      getTransactionList();
    });



    return () => {
      willFocusSubscription;
    }
  }, [])

  const height = Dimensions.get('window').height
  const width = Dimensions.get('window').width

  const getTransactionList = () => {
    console.log(userDetail)
    setLoading(true);
    const data = { type_id: '' }
    Post('my-transactions', data).then((res) => {
      if (res.status == 200) {
        setTransList(res.list)
        console.log(res.list[0])
      }
      // setTransList(res.list)
      setLoading(false);


    }, err => {
      setLoading(false);
      console.log(err)
    })
  }


  const addmoney = () => {
    setLoading(true);
    const d = {
      amount: 1000,
      token: 2,
      message: 'money added to wallet'
    }
    Post('add-money', d).then((res) => {
      if (res.status == 200) {
        // setShowThanks(true);
      }
      // Toaster(res.message)
      setLoading(false);
    }, err => {
      // Toaster(res.message)
      setLoading(false);
      console.log(err)
    })
  }

  const filterUserTransaction = (item) => {
    props.navigation.navigate('transactionbyId', JSON.stringify(item))
  }

  const imges = [
    <Image source={require('../../../Assets/Images/Banner.png')} style={{ height: 200, width, overflow: 'hidden' }} />,
    <Image source={require('../../../Assets/Images/Banner.png')} style={{ height: 200, width, overflow: 'hidden' }} />,
    <Image source={require('../../../Assets/Images/Banner.png')} style={{ height: 200, width, overflow: 'hidden' }} />
  ]


  const _renderItem = ({ item, index }) => {
    return (
      <Image source={require('../../../Assets/Images/Banner.png')} style={{ height: 200, width, overflow: 'hidden' }} />
    );
  }


  const onChangeSearch = query => setSearchQuery(query);
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: '#FCFCFC', position: 'relative' }}>
      <Spinner color={'#fff'} visible={loading} />
      <ScrollView >
        {/* <View >
          <Searchbar
            style={Styles.shadowProp}
            inputStyle={{ height: 40 }}
            placeholder="Search"
            onChangeText={onChangeSearch}
            value={searchQuery}
          />
        </View> */}

        <View style={{ overflow: 'hidden' }}>
          <Carousel
            data={imges}
            renderItem={_renderItem}
            sliderWidth={width}
            itemWidth={width}
            autoplay={true}
            loop={true}
            // autoplayDelay={1000}
            autoplayInterval={3000}
          />
          {/* <ScrollView horizontal={true} style={{overflow:'hidden'}}>
            <Image source={require('../../../Assets/Images/Banner.png')}  style={{ height: 200,width,overflow:'hidden' }} />
            <Image source={require('../../../Assets/Images/Banner.png')}  style={{ height: 200,width,overflow:'hidden' }} />
            <Image source={require('../../../Assets/Images/Banner.png')}  style={{ height: 200,width,overflow:'hidden' }} />
          </ScrollView> */}
        </View>



        <View style={{ flexDirection: 'row', flexWrap: 'wrap', padding: 10 }}>

          <TouchableOpacity style={Styles.card} onPress={() => {
            props.navigation.navigate('transactions')
            // props.navigation.navigate('checkBalance')
          }}>
            <View style={Styles.cardShadow}>
              <Image source={require('../../../Assets/Images/employee.png')} resizeMode='contain' style={{ height: 30, }} />
              <Text style={{ fontFamily: 'Montserrat-SemiBold', marginTop: 10, fontSize: 10, color: '#1D1D1D' }}>Transactions</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={Styles.card} onPress={() => { props.navigation.navigate('Money', { screen: 'splitMoney' }) }}>
            <View style={Styles.cardShadow}>
              <Image source={require('../../../Assets/Images/secure-payment.png')} resizeMode='contain' style={{ height: 30, }} />
              <Text style={{ fontFamily: 'Montserrat-SemiBold', marginTop: 10, fontSize: 10, color: '#1D1D1D' }}>Split Money</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={Styles.card} onPress={() => { props.navigation.navigate('Money', { screen: 'requestMoney' }) }}>
            <View style={Styles.cardShadow}>
              <Image source={require('../../../Assets/Images/hand.png')} resizeMode='contain' style={{ height: 30, }} />
              <Text style={{ fontFamily: 'Montserrat-SemiBold', marginTop: 10, fontSize: 10, color: '#1D1D1D' }}>Request Money</Text>
            </View>
          </TouchableOpacity>

        </View>

        {/* <View style={{ flexDirection: 'row', flexWrap: 'wrap', padding: 10 }}>
        
          <View style={Styles.card}>
            <View style={Styles.cardShadow}>
              <Image source={require('../../../Assets/Images/secure-payment.png')} resizeMode='contain' style={{ height: 30, }} />
              <Text style={{ fontFamily: 'Montserrat-SemiBold', marginTop: 10, fontSize: 10 }}>Secure Payment</Text>
            </View>
          </View>

          <View style={Styles.card}>
            <View style={Styles.cardShadow}>
              <Image source={require('../../../Assets/Images/coupon.png')} resizeMode='contain' style={{ height: 30, }} />
              <Text style={{ fontFamily: 'Montserrat-SemiBold', marginTop: 10, fontSize: 10 }}>Discount Coupons</Text>
            </View>
          </View>

          <View style={Styles.card}>
            <View style={Styles.cardShadow}>
              <Image source={require('../../../Assets/Images/employee.png')} resizeMode='contain' style={{ height: 30, }} />
              <Text style={{ fontFamily: 'Montserrat-SemiBold', marginTop: 10, fontSize: 10 }}>Transactions</Text>
            </View>
          </View>

        </View> */}


        <View style={{ paddingBottom: 70 }}>
          <Text style={{ fontFamily: 'Montserrat-Bold', margin: 20, color: '#000000', fontWeight: '700', fontSize: 16 }}>Recent Transactions</Text>

          <View >
            {transList != undefined && transList.map((item) => (
              <View key={item.id} style={{ flexDirection: 'row', paddingHorizontal: 20, borderBottomColor: '#F6F6F6', borderBottomWidth: 1, paddingVertical: 10 }}>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                  <TouchableOpacity onPress={() => { filterUserTransaction(item) }}>
                    {userDetail.user != undefined && userDetail.user.id != item.to_user.id && <Avatar.Text size={30} label={item?.to_user.name != undefined && item?.to_user.name.charAt(0) || item?.to_user.phone.charAt(0)} labelStyle={{ fontFamily: 'Montserrat-Bold' }} style={{ backgroundColor: '#000000' }} />}
                    {userDetail.user != undefined && userDetail.user.id == item.to_user.id && <Avatar.Text size={30} label={item?.from_user.name != undefined && item?.from_user.name.charAt(0) || item?.from_user.phone.charAt(0)} labelStyle={{ fontFamily: 'Montserrat-Bold' }} style={{ backgroundColor: '#000000' }} />}
                  </TouchableOpacity>

                  <View style={{ marginLeft: 10 }}>
                    {userDetail.user != undefined && userDetail.user.id != item.to_user.id && <Text style={{ fontFamily: 'Montserrat-SemiBold', color: '#000000' }}>{item?.to_user?.name || item?.to_user.phone}</Text>}
                    {userDetail.user != undefined && userDetail.user.id == item.to_user.id && <Text style={{ fontFamily: 'Montserrat-SemiBold', color: '#000000' }}>{item?.from_user?.name || item?.from_user.phone}</Text>}
                    <Text style={{ fontFamily: 'Montserrat-SemiBold', color: 'lightgrey' }}>at {moment(item.created_at).format('h:mm a')}</Text>
                  </View>
                </View>
                <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                  {userDetail.user != undefined && userDetail.user.id == item.to_user.id && <Text style={{ fontFamily: 'Montserrat-Bold', color: '#1DB00D' }}>+ CHF {item.amount}</Text>}
                  {userDetail.user != undefined && userDetail.user.id != item.to_user.id && <Text style={{ fontFamily: 'Montserrat-Bold', color: '#FF0000' }}>- CHF {item.amount}</Text>}
                  <Text style={{ fontFamily: 'Montserrat-SemiBold', color: 'lightgrey' }}>{moment(item.created_at).format('MMM DD,YYYY')}</Text>
                </View>
              </View>
            ))}
          </View>
        </View>
      </ScrollView>
      <View style={{ position: 'absolute', bottom: 0, paddingVertical: 10, paddingHorizontal: 5, flexDirection: 'row', width: '100%' }}>

        <TouchableOpacity style={Styles.sendQr} onPress={() => { props.navigation.navigate('sendMoney') }}>
          <FontAwesome5 name='wallet' size={22} color='#C65859' />
          <Text style={{ color: '#C65859', fontSize: 18, fontFamily: 'Montserrat-Bold', marginLeft: 10 }}>SEND</Text>
        </TouchableOpacity>
        <TouchableOpacity style={Styles.sendQr} onPress={() => {
          props.navigation.navigate('Scan')
          // props.navigation.navigate('myQR') 
        }}>
          <FontAwesome5 name='qrcode' size={22} color='#E98607' />
          <Text style={{ color: '#E98607', fontSize: 18, fontFamily: 'Montserrat-Bold', marginLeft: 10 }}>QR CODE</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default Home;
