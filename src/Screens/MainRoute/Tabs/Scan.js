import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
  View
} from 'react-native';
import React from 'react';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';
import { Avatar } from 'react-native-paper';

const Scan = (props) => {

  const onSuccess = e => {
    console.log(e)
    props.navigation.navigate('sendMoney',{qrValue:JSON.stringify(e)})
    // alert(JSON.stringify(e))/
    // Linking.openURL(e.data).catch(err =>
    //   console.error('An error occured', err)
    // );
  };

  return (
    <QRCodeScanner
      onRead={onSuccess}
      showMarker={true}
      markerStyle={{ height: 240, width: 240, borderRadius: 20, borderColor: '#000000' }}
      reactivate={true}
      reactivateTimeout={5000}
      fadeIn={true}
      bottomContent={
        <View style={{ position: 'absolute', bottom: 20, right: 20 }}>
          <TouchableOpacity onPress={() => { props.navigation.navigate('myQR') }}>
            <Avatar.Text size={50} label="My QR" labelStyle={{ fontFamily: 'Montserrat-Bold', color: '#fff', fontSize: 10 }} style={{ backgroundColor: '#000000' }} />
          </TouchableOpacity>
        </View>
      }
    />
  );
};

const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777'
  },
  textBold: {
    fontWeight: '500',
    color: '#000'
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)'
  },
  buttonTouchable: {
    padding: 16
  }
});


export default Scan;
