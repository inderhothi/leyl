import { View, Text, Button, TouchableOpacity, Image, StyleSheet, TextInput, Keyboard } from 'react-native';
import React, { useState, useEffect, useRef } from 'react';
import { Avatar } from 'react-native-paper';
import Styles from './Styles';
import IonIcon from 'react-native-vector-icons/Ionicons';
import { GetApi, Post } from '../../../../Helpers/Service';
import Spinner from '../../../Styles/Spinner'
import { checkForEmptyKeys, checkNumber, checkEmail } from '../../../../serviceProvider/InputsNullChecker'
import Toaster from '../../../Styles/Toaster'
import AsyncStorage from '@react-native-async-storage/async-storage';


const Account = (props) => {

  const [edit, setEdit] = useState(false);
  const [profileObj, setProfileObj] = useState({
    name: '',
    phone: '',
    email: ''
  });
  const [loading, setLoading] = useState(false);
  const [userDetail, setUserDetail] = useState({});
  const inputRef = useRef()
  useEffect(() => {
    getUserDetail();
    const willFocusSubscription = props.navigation.addListener('focus', () => {
      getUserDetail();
    });
    return () => {
      willFocusSubscription;
    }
  }, [])

  useEffect(() => {
    inputRef.current.focus();
  }, [edit])

  useEffect(() => {
    const keyboardDidShowListener = Keyboard.addListener("keyboardDidShow", () => {
      // setKeyboardVisible(true)
    });

    const keyboardDidHideListener = Keyboard.addListener("keyboardDidHide", () => {
      // setKeyboardVisible(false)
      // setReplyAailable(false);
    });
    return () => {
      keyboardDidHideListener.remove();
      keyboardDidShowListener.remove();

    };
  }, [edit])

  const getUserDetail = () => {
    setLoading(true);
    GetApi('profile').then((res) => {
      console.log(res)
      if (res.status == 200) {
        setUserDetail(res.details)
        setProfileObj({
          name: res?.details?.name || '',
          phone: res?.details?.phone || '',
          email: res?.details?.email || ''
        })
      }
      setLoading(false);
    }, err => {
      setLoading(false);
      console.log(err)
    })
  }

  const updateProfile = () => {
    if (!checkNumber(profileObj.phone) || profileObj.phone.length < 10) {
      return Toaster('The phone number does not valid.')
    }
    if (!checkEmail(profileObj.email)) {
      return Toaster('The email does not valid.')
    }
    setLoading(true);
    Post('update-profile', profileObj).then((res) => {
      console.log(res)
      if (res.status == 200) {
        setEdit(false)
        setUserDetail(res.details)
        setProfileObj({
          name: res?.details?.name || '',
          phone: res?.details?.phone || '',
          email: res?.details?.email || ''
        })
      }
      setLoading(false);
    }, err => {
      setLoading(false);
      console.log(err)
    })


  }

  const openKeyboard = () => {
    setEdit(true)
    inputRef.current.focus()
  }
  return (
    <View style={{ flex: 1, backgroundColor: '#fff' }} keyboardShouldPersistTaps='always'>
      <Spinner color={'#fff'} visible={loading} />
      <View >
        <View style={[Styles.card2]} >

          <View style={[Styles.cardShadow2, { height: 200, paddingHorizontal: 20, position: 'relative' }]}>
            {!edit && <TouchableOpacity style={{ position: 'absolute', top: 10, right: 20, }} onPress={() => { openKeyboard() }}>
              <Text style={{ fontFamily: 'Montserrat-SemiBold', fontSize: 16, color: 'green' }}>Edit</Text>
            </TouchableOpacity>}
            {edit && <TouchableOpacity style={{ position: 'absolute', top: 10, right: 20 }} onPress={() => { updateProfile() }}>
              <Text style={{ fontFamily: 'Montserrat-SemiBold', fontSize: 16, color: 'green' }}>Save</Text>
            </TouchableOpacity>}
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flex: 6, justifyContent: 'center', alignItems: 'flex-start', width: '100%' }}>
                <TextInput
                  value={profileObj.name}
                  editable={edit}
                  placeholder={edit ? 'Enter Your name' : ''}
                  onChangeText={(text) => { setProfileObj({ ...profileObj, name: text }) }}
                  style={{ fontFamily: 'Montserrat-SemiBold', fontSize: 20, color: '#1D1D1D', padding: 0, width: '100%' }}
                  ref={inputRef}
                  blurOnSubmit={false}
                // autoFocus={true}
                ></TextInput>

                <TextInput
                  keyboardType='numeric'
                  value={profileObj.phone}
                  editable={edit}
                  placeholder={edit ? 'Enter Your phone' : ''}
                  onChangeText={(text) => { setProfileObj({ ...profileObj, phone: text }) }}
                  style={{ fontFamily: 'Montserrat-SemiBold', fontSize: 16, color: '#1D1D1D', margin: 0, padding: 0, width: '100%' }}
                ></TextInput>

                <TextInput
                  value={profileObj.email}
                  editable={edit}
                  placeholder={edit ? 'Enter Your email' : ''}
                  onChangeText={(text) => { setProfileObj({ ...profileObj, email: text }) }}
                  style={{ fontFamily: 'Montserrat-SemiBold', fontSize: 16, color: '#1D1D1D', margin: 0, padding: 0, width: '100%' }}
                ></TextInput>

              </View>
              <View style={{ flex: 4, justifyContent: 'center', alignItems: 'flex-end' }}>
                <View style={{ height: 80, width: 80, position: 'relative' }}>
                  <Avatar.Text size={80} label={profileObj.name != '' ? profileObj.name.charAt(0) : profileObj.phone.charAt(0)} />
                  <TouchableOpacity style={{ position: 'absolute', bottom: 0, right: 0 }}
                    onPress={() => { props.navigation.navigate('myQR') }} >
                    <Avatar.Image size={30} source={require('../../../Assets/Images/code.png')} />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </View>
      </View >

      <View style={{ marginTop: 10 }}>
        <TouchableOpacity style={Styles.card2} onPress={() => {
          props.navigation.navigate('checkBalance')
        }}>
          <View style={Styles.cardShadow2}>
            <Image source={require('../../../Assets/Images/employee.png')} resizeMode='contain' style={{ height: 30, }} />
            {/* <IonIcon name='card' size={30} color='#1D1D1D' /> */}
            <Text style={{ fontFamily: 'Montserrat-SemiBold', marginTop: 10, fontSize: 14, color: '#1D1D1D' }}>View Balance</Text>
          </View>
        </TouchableOpacity>
      </View>

      <View style={{ marginTop: 1 }}>
        <TouchableOpacity style={Styles.card2} onPress={() => {
          props.navigation.navigate('transactions')
        }}>
          <View style={Styles.cardShadow2}>
            <Image source={require('../../../Assets/Images/employee.png')} resizeMode='contain' style={{ height: 30, }} />
            <Text style={{ fontFamily: 'Montserrat-SemiBold', marginTop: 10, fontSize: 14, color: '#1D1D1D' }}>Settings</Text>
          </View>
        </TouchableOpacity>
      </View>

      <View style={{ marginTop: 1 }}>
        <TouchableOpacity style={Styles.card2} onPress={() => {
          props.navigation.navigate('changeMpin')
        }}>
          <View style={Styles.cardShadow2}>
            <Image source={require('../../../Assets/Images/employee.png')} resizeMode='contain' style={{ height: 30, }} />
            <Text style={{ fontFamily: 'Montserrat-SemiBold', marginTop: 10, fontSize: 14, color: '#1D1D1D' }}>Change Mpin</Text>
          </View>
        </TouchableOpacity>
      </View>

      <View style={{ marginTop: 1 }}>
        <TouchableOpacity style={Styles.card2} onPress={async () => {
          await AsyncStorage.removeItem('userDetail')
          props.navigation.replace('loginMpin')

        }}>
          <View style={Styles.cardShadow2}>
            <Image source={require('../../../Assets/Images/employee.png')} resizeMode='contain' style={{ height: 30, }} />
            <Text style={{ fontFamily: 'Montserrat-SemiBold', marginTop: 10, fontSize: 14, color: '#1D1D1D' }}>Logout</Text>
          </View>
        </TouchableOpacity>
      </View>

      {/* <View style={{ marginTop: 10 }}>
        <TouchableOpacity style={Styles.card2} onPress={() => {
          props.navigation.navigate('transactions')
        }}>
          <View style={Styles.cardShadow2}>
            <Image source={require('../../../Assets/Images/employee.png')} resizeMode='contain' style={{ height: 30, }} />
            <Text style={{ fontFamily: 'Montserrat-SemiBold', marginTop: 10, fontSize: 10, color: '#1D1D1D' }}>Transactions</Text>
          </View>
        </TouchableOpacity>
      </View> */}


    </View >
  );
};



export default Account;
