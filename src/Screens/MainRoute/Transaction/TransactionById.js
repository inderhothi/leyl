import { View, Text, ScrollView, SafeAreaView, TouchableOpacity } from 'react-native';
import React, { useEffect, useState } from 'react';
import { Avatar } from 'react-native-paper';
import { Post } from '../../../../Helpers/Service';
import Spinner from '../../../Styles/Spinner'
import moment from 'moment';
import AsyncStorage from '@react-native-async-storage/async-storage';
import IonIcon from 'react-native-vector-icons/Ionicons';

const TransactionById = (props) => {
    const [transList, setTransList] = useState([]);
    const [loading, setLoading] = useState(false);
    const [userDetail, setUserDetail] = useState({});
    const [showClose, setShowClose] = useState(false);
    const [transactionDetail, setTransactionDetail] = useState({});



    useEffect(() => {



        (async () => {
            const usedetail = await AsyncStorage.getItem('userDetail');
            if (usedetail) {
                setUserDetail(JSON.parse(usedetail))
                if (props.route.params != undefined) {
                    filterUserTransaction(JSON.parse(props.route.params));
                }
            }
        })();

        console.log(transactionDetail)
        return (() => {
            setTransactionDetail({})
        })
    }, [props.route.params])



    const filterUserTransaction = (item) => {
        setTransactionDetail(item)
        console.log(item)
        setLoading(true);
        const data = { user_id: userDetail?.user?.id != item.to_user.id ? item.to_user.id : item.from_user.id }
        console.log(data)
        Post('transaction-history', data).then((res) => {
            if (res.status == 200) {
                setTransList(res.list)
                setShowClose(true)
            }
            // setTransList(res.list)
            setLoading(false);
            console.log(transList)

        }, err => {
            setLoading(false);
            console.log(err)
        })
    }


    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FCFCFC' }}>
            <Spinner color={'#fff'} visible={loading} />
            <ScrollView >
                <View >
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 1 }}>
                            <Text style={{ color: '#1D1D1D', fontSize: 20, margin: 10, fontWeight: 'bold' }}>{userDetail?.user?.id != transactionDetail?.to_user?.id ? transactionDetail?.to_user?.name : transactionDetail?.from_user?.name}</Text>
                        </View>
                    </View>

                    {transList != undefined && transList.map((item) => (
                        <View key={item.id} style={{ flexDirection: 'row', paddingHorizontal: 20, borderBottomColor: '#F6F6F6', borderBottomWidth: 1, paddingVertical: 10 }}>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>

                                <View style={{ marginLeft: 10 }}>

                                    <Text style={{ fontFamily: 'Montserrat-SemiBold', color: '#000000' }}>{item?.message || 'transaction'}</Text>
                                    <Text style={{ fontFamily: 'Montserrat-SemiBold', color: 'lightgrey' }}>at {moment(item.created_at).format('h:mm a')}</Text>
                                </View>
                            </View>
                            <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                                {userDetail.user != undefined && userDetail.user.id == item.to_user_id && <Text style={{ fontFamily: 'Montserrat-Bold', color: '#1DB00D' }}>+ CHF {item.amount}</Text>}
                                {userDetail.user != undefined && userDetail.user.id != item.to_user_id && <Text style={{ fontFamily: 'Montserrat-Bold', color: '#FF0000' }}>- CHF {item.amount}</Text>}
                                <Text style={{ fontFamily: 'Montserrat-SemiBold', color: 'lightgrey' }}>{moment(item.created_at).format('MMM DD,YYYY')}</Text>
                            </View>
                        </View>))}
                </View>
            </ScrollView>
        </SafeAreaView>
    );
}

export default TransactionById