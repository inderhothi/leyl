import { View, Text, ScrollView, SafeAreaView, TouchableOpacity } from 'react-native';
import React, { useEffect, useState } from 'react';
import { Avatar } from 'react-native-paper';
import { Post } from '../../../../Helpers/Service';
import Spinner from '../../../Styles/Spinner'
import moment from 'moment';
import AsyncStorage from '@react-native-async-storage/async-storage';
import IonIcon from 'react-native-vector-icons/Ionicons';


const Transactions = (props) => {
    const [transList, setTransList] = useState([]);
    const [loading, setLoading] = useState(false);
    const [userDetail, setUserDetail] = useState({});
    const [showClose, setShowClose] = useState(false);



    useEffect(() => {
        getTransactionList();

        (async () => {
            const usedetail = await AsyncStorage.getItem('userDetail');
            if (usedetail) {
                setUserDetail(JSON.parse(usedetail))
            }
        })();
    }, [])

    const getTransactionList = () => {
        setLoading(true);
        const data = { type_id: '' }
        Post('my-transactions', data).then((res) => {
            if (res.status == 200) {
                setTransList(res.list)
            }
            setShowClose(false)
            // setTransList(res.list)
            setLoading(false);
            console.log(res.list)

        }, err => {
            setLoading(false);
            console.log(err)
        })
    }

    const filterUserTransaction = (item) => {
        const data = { user_id: userDetail.user.id != item.to_user.id ? item.to_user.id : item.from_user.id }
        props.navigation.navigate('transactionbyId', JSON.stringify(item))
    }


    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FCFCFC' }}>
            <Spinner color={'#fff'} visible={loading} />
            <ScrollView >
                <View >
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 1 }}>
                            <Text style={{ color: '#1D1D1D', fontSize: 20, margin: 10, fontWeight: 'bold' }}>All Transactions</Text>
                        </View>
                    </View>

                    {transList != undefined && transList.map((item) => (
                        <View key={item.id} style={{ flexDirection: 'row', paddingHorizontal: 20, borderBottomColor: '#F6F6F6', borderBottomWidth: 1, paddingVertical: 10 }}>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <TouchableOpacity onPress={() => { filterUserTransaction(item) }}>
                                    {userDetail.user != undefined && userDetail.user.id != item.to_user.id &&
                                        <Avatar.Text size={30}
                                            label={item?.to_user.name != undefined && item?.to_user.name.charAt(0) || item?.to_user.phone.charAt(0)}
                                            labelStyle={{ fontFamily: 'Montserrat-Bold' }}
                                            style={{ backgroundColor: '#000000' }}

                                        />}
                                    {userDetail.user != undefined && userDetail.user.id == item.to_user.id &&
                                        <Avatar.Text size={30}
                                            label={item?.from_user.name != undefined && item?.from_user.name.charAt(0) || item?.from_user.phone.charAt(0)}
                                            labelStyle={{ fontFamily: 'Montserrat-Bold' }}
                                            style={{ backgroundColor: '#000000' }}
                                        />}
                                </TouchableOpacity>
                                <View style={{ marginLeft: 10 }}>
                                    {userDetail.user != undefined && userDetail.user.id != item.to_user.id && <Text style={{ fontFamily: 'Montserrat-SemiBold', color: '#000000' }}>{item?.to_user?.name || item?.to_user.phone}</Text>}
                                    {userDetail.user != undefined && userDetail.user.id == item.to_user.id && <Text style={{ fontFamily: 'Montserrat-SemiBold', color: '#000000' }}>{item?.from_user?.name || item?.from_user.phone}</Text>}
                                    <Text style={{ fontFamily: 'Montserrat-SemiBold', color: 'lightgrey' }}>at {moment(item.created_at).format('h:mm a')}</Text>
                                </View>
                            </View>
                            <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                                {userDetail.user != undefined && userDetail.user.id == item.to_user.id && <Text style={{ fontFamily: 'Montserrat-Bold', color: '#1DB00D' }}>+ CHF {item.amount}</Text>}
                                {userDetail.user != undefined && userDetail.user.id != item.to_user.id && <Text style={{ fontFamily: 'Montserrat-Bold', color: '#FF0000' }}>- CHF {item.amount}</Text>}
                                <Text style={{ fontFamily: 'Montserrat-SemiBold', color: 'lightgrey' }}>{moment(item.created_at).format('MMM DD,YYYY')}</Text>
                            </View>
                        </View>))}
                </View>
            </ScrollView>
        </SafeAreaView>
    );
};

export default Transactions;
