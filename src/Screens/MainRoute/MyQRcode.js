import { View, Text, Image, TouchableOpacity, StyleSheet } from 'react-native';
import React, { useEffect, useState } from 'react';
import { Avatar } from 'react-native-paper';
import LinearGradient from "react-native-linear-gradient";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import QRCode from 'react-native-qrcode-svg';
import AsyncStorage from '@react-native-async-storage/async-storage';

const MyQRcode = (props) => {
    const [userdetail, setUserdetail] = useState('');

    useEffect(() => {
        userd();
    }, [])

    const userd = async () => {
        let data = await AsyncStorage.getItem('userDetail');
        setUserdetail(JSON.parse(data).user?.phone)
        // console.log("JSON.parse(data)", data.user?.phone)
    }
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#FFFFFF' }}>
            <Avatar.Text size={40} label={userdetail != undefined && userdetail.charAt(0)} labelStyle={{ fontFamily: 'Montserrat-Bold', color: '#fff' }} style={{ backgroundColor: '#9519C8' }} />
            <Text style={{ color: '#000000', marginTop: 5, fontSize: 18, fontFamily: 'montserrat-Light' }}>{userdetail != undefined && userdetail}</Text>
            <Text style={{ color: 'grey', fontFamily: 'montserrat-Light', opacity: 0.8, marginBottom: 40 }}> verified User</Text>
            <QRCode
                value={JSON.stringify(userdetail)}
                size={260}
            />
            {/* <Image source={require('../../Assets/Images/code.png')} style={{ height: 260, width: 260, marginTop: 40 }} /> */}
            <TouchableOpacity style={{ width: '80%', height: 45, marginTop: 40, borderRadius: 10 }}
                onPress={() => { props.navigation.navigate('Scan') }}>
                <LinearGradient
                    colors={['#E98607', '#9013D1']} style={styles.loginLiner}>
                    <FontAwesome5 name='camera' size={20} color='#fff' />
                    <Text style={{ color: '#fff', fontSize: 18, fontFamily: 'Montserrat-Light', fontWeight: '700', marginLeft: 10 }}>Scan QR Code</Text>
                </LinearGradient>
            </TouchableOpacity>

        </View>
    );
};

const styles = StyleSheet.create({
    loginLiner: {
        width: '100%',
        height: 45,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        flexDirection: 'row'
    },
});


export default MyQRcode;
