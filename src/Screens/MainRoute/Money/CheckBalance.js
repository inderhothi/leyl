import { View, Text } from 'react-native'
import React, { useState, useEffect } from 'react'
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toaster from '../../../Styles/Toaster';
import Spinner from '../../../Styles/Spinner';
import { GetApi, Post } from '../../../../Helpers/Service';

const CheckBalance = (props) => {
    const [balance, setbalance] = useState();
    const [loading, setLoading] = useState(false);
    const [userDetail, setUserDetail] = useState({});

    useEffect(() => {
        // checkBalance();

        (async () => {
            const usedetail = await AsyncStorage.getItem('userDetail');
            if (usedetail) {
                setUserDetail(JSON.parse(usedetail))
            }
        })();
        // addmoney();

        const willFocusSubscription = props.navigation.addListener('focus', () => {
            checkBalance();
        });


        return () => {
            willFocusSubscription;
            // checkBalance();
        }
    }, [])


    const checkBalance = () => {
        setLoading(true);
        GetApi('my-balance').then((res) => {
            console.log(res)
            if (res.status == 200) {
                setbalance(res.balance)
            }
            setLoading(false);
        }, err => {
            setLoading(false);
            console.log(err)
        })
    }
    return (
        <View style={{ flex: 1, backgroundColor: '#000000', padding: 40 }}>
            <Spinner color={'#fff'} visible={loading} />
            <Text style={{ color: '#fff', fontSize: 20 }}>Your Accont Balance</Text>
            <Text style={{ color: '#fff', fontSize: 18, marginTop: 20 }}>Saving account</Text>
            <Text style={{ color: '#fff', fontSize: 16, }}>XXXXXXXXXXXX0446</Text>
            <Text style={{ color: '#fff', fontSize: 18, marginTop: 20 }}>Account balance</Text>
            <Text style={{ color: '#fff', fontSize: 16, }}>CHF {balance}</Text>
        </View>
    )
}

export default CheckBalance