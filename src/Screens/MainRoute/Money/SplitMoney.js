import { View, Text, Image, TouchableOpacity, StyleSheet, TextInput, PermissionsAndroid, ScrollView, Dimensions, Keyboard, RefreshControl } from 'react-native';
import React, { useEffect, useState } from 'react';
import LinearGradient from "react-native-linear-gradient";
import { selectContactPhone } from 'react-native-select-contact';
import Toaster from '../../../Styles/Toaster';
import { Searchbar, Card, Avatar } from 'react-native-paper';
import IonIcon from 'react-native-vector-icons/Ionicons';
import Spinner from '../../../Styles/Spinner';
import { Post } from '../../../../Helpers/Service';

const SplitMoney = (props) => {
    const [loading, setLoading] = useState(false);
    const [showThanks, setShowThanks] = useState(false);
    const [phoneBookDetail, setPhoneBookDetail] = useState(false);
    const [contact, setContact] = useState('');
    const [refreshing, setRefreshing] = React.useState(false);
    // const [dollar, setDollar] = useState('$');
    const [pay, setPay] = useState({
        amount: '',
        name: [],
        message: ''
    })
    const [Amount, setAmount] = useState()
    const height = Dimensions.get('window').height

    useEffect(() => {
        const keyboardDidShowListener = Keyboard.addListener("keyboardDidShow", () => {
            // setKeyboardVisible(true)
        });

        const keyboardDidHideListener = Keyboard.addListener("keyboardDidHide", () => {
            console.log(contact)
            if (contact != '' && contact.length == 10) {
                checkNumber(contact, contact)
                // pay.name.push({
                //     name: contact,
                //     number: contact
                // })
                // setPay(pay)
                if (pay.amount != '' && pay.name.length > 0) {
                    setAmount(pay?.amount.replace('CHF ', '') || 0 / pay.name.length);
                }
                setContact('')
                console.log(pay.name)
            }

        });
        return () => {
            keyboardDidHideListener.remove();
            keyboardDidShowListener.remove();
        };
    }, [contact])

    useEffect(() => {
        const keyboardDidShowListener = Keyboard.addListener("keyboardDidShow", () => {
            if (pay.amount.length == 0) {
                setPay({ ...pay, amount: 'CHF ' })
            }
        });
        return () => {
            keyboardDidShowListener.remove();
        };
    }, [pay.amount.length])

    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    }

    const getPhoneNumber = async () => {
        console.log('selection')
        const permition = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
            {
                'title': 'Contacts',
                'message': 'This app would like to view your contacts.',
                'buttonPositive': 'Please accept bare mortal'
            }
        )
        console.log('selection', permition)
        if (permition == 'granted') {
            selectContactPhone().then(selection => {
                console.log(selection)
                if (!selection) {
                    return null;
                }

                let { contact, selectedPhone } = selection;
                let x = selectedPhone.number.replace(/\D/g, '');
                if (x.length == 12) {
                    x = x.substring(2)
                }
                checkNumber(contact.name, x)


                console.log(contact);
                setPhoneBookDetail(selection)
            })
        }

    }

    const checkNumber = (name, number) => {
        setLoading(true);
        const data = { phone: number || '' }
        console.log(data)
        Post('get-user', data).then((res) => {
            setLoading(false);
            if (res.user != undefined) {
                pay.name.push({
                    name,
                    number,
                    user_id: res.user.id,
                })
                console.log(pay)
                setPay(pay)
                setRefreshing(true);
                const amount = pay.amount.replace('CHF ', '')
                setAmount(amount / pay.name.length)
                wait(500).then(() => setRefreshing(false));
            } else {
                Toaster(res.message)
            }
        }, err => {
            Toaster(res.message)
            setLoading(false);
            console.log(err)
        })
    }


    const sendMoney = () => {
        console.log(Amount)
        if (Amount <= 0) {
            Toaster('please enter amount then proceed')
            return;
        }

        if (pay.message.length == 0) {
            Toaster('please add a message then proceed')
            return;
        }

        setLoading(true);

        pay.name.forEach((ele, index) => {
            const d = {
                amount: Amount || '',
                user_id: ele.user_id,
                message: pay?.message || ''
            }

            Post('transfer-money', d).then((res) => {
                if (res.status == 200) {
                    setShowThanks(true);
                }
                Toaster(res.message)
            }, err => {
                Toaster(res.message)
                setLoading(false);
                console.log(err)
            })

            if (pay.name.length == index + 1) {
                setLoading(false);
            }
        })

    }

    const removeItem = (number, i) => {
        var filtered = pay.name.filter(function (value, index) {
            return index != i;
        });
        const amount = pay.amount.replace('CHF ', '')
        setAmount(amount / filtered.length)
        setPay({ ...pay, name: filtered })
        setRefreshing(true);
        wait(500).then(() => setRefreshing(false));
    }


    return (
        <>
            <ScrollView estedScrollEnabled={true} contentContainerStyle={{ minHeight: height, backgroundColor: '#000000' }}>
                <Spinner color={'#fff'} visible={loading} />
                {!showThanks ? <ScrollView style={{ minHeight: height }}>
                    <View style={{ flex: 1, alignItems: 'center', backgroundColor: '#000000', minHeight: height }}>
                        <Text style={[styles.contactLabel, { width: '100%', textAlign: 'left', padding: 20 }]}>Amount</Text>
                        {/* <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={styles.money}>$</Text>
                        <TextInput keyboardType='numeric' value={pay.amount} onChangeText={(text) => { setPay({ ...pay, amount: text }); setAmount(parseInt(text) / pay.name.length) }} style={styles.money} />
                    </View> */}
                        <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                            {/* <Text style={styles.money}>$</Text> */}
                            <TextInput
                                keyboardType='numeric'
                                placeholder={'CHF 00'} placeholderTextColor={'grey'}
                                // value={`${dollar} ${pay.amount}`}
                                value={pay.amount}
                                onChangeText={(text) => { setPay({ ...pay, amount: text }); setAmount(parseInt(text.replace('CHF ', '')) / pay.name.length) }}
                                style={styles.money} />
                        </View>
                        {/* <TextInput value={`${dollar} ${pay.amount}`} onChangeText={(text) => { setPay({ ...pay, amount: text }); setDollar('') }} style={styles.money} /> */}
                        <View style={{ width: '100%', padding: 20, marginTop: 20 }}>
                            <View style={[styles.inputView, { position: 'relative' }]}>
                                <Text style={styles.contactLabel}>Contact</Text>
                                <TextInput value={contact} onChangeText={(text) => { setContact(text) }} placeholder='Enter Name or Mobile Number' placeholderTextColor={'grey'} style={{ color: '#fff' }} />
                                <TouchableOpacity style={{ position: 'absolute', right: 0, height: 40, width: 30, top: 30 }} onPress={() => { getPhoneNumber() }} >
                                    <Image source={require('../../../Assets/Images/contact-book.png')} style={{ height: 20, width: 18 }} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.inputView}>
                                <Text style={styles.contactLabel}>Message</Text>
                                <TextInput value={pay.message} onChangeText={(text) => { setPay({ ...pay, message: text }) }} placeholder='Add a Message' multiline={true} placeholderTextColor={'grey'} style={{ color: '#fff' }} />
                            </View>
                            <Text style={styles.contactLabel}>Split</Text>
                            <View style={{ marginTop: 10 }} refreshControl={
                                <RefreshControl
                                    refreshing={refreshing}
                                // onRefresh={onRefresh}
                                />}>
                                {
                                    pay.name != undefined && pay.name.map((item, index) => (
                                        <View key={index} style={{ flexDirection: 'row', paddingHorizontal: 20, borderBottomColor: '#1D1D1D', borderBottomWidth: 1, paddingVertical: 10 }}>
                                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                                <TouchableOpacity style={{ marginRight: 10 }} onPress={() => { removeItem(item.number, index) }}>
                                                    <IonIcon name='close-circle-outline' size={20} color='#fff' />
                                                </TouchableOpacity>
                                                <Avatar.Text size={30} label={item.name.charAt(0)} labelStyle={{ fontFamily: 'Montserrat-Bold' }} style={{ backgroundColor: '#fff' }} />
                                                <View style={{ marginLeft: 10 }}>
                                                    <Text style={{ fontFamily: 'Montserrat-SemiBold', color: '#fff' }}>{item.name}</Text>
                                                    <Text style={{ fontFamily: 'Montserrat-SemiBold', color: 'lightgrey' }}>{item.number}</Text>
                                                </View>
                                            </View>
                                            <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                                                <Text style={{ fontFamily: 'Montserrat-Bold', color: '#1DB00D' }}>+CHF {Amount}</Text>
                                            </View>
                                        </View>
                                    ))
                                }
                            </View>
                        </View>
                        <TouchableOpacity style={{ width: '90%', height: 55, marginTop: 40, borderRadius: 10 }}
                            onPress={() => { sendMoney() }}>

                            <LinearGradient
                                colors={['#E98607', '#9013D1']} style={styles.loginLiner}>
                                <Text style={{ color: '#000000', fontSize: 18, fontFamily: 'Montserrat-Light', fontWeight: '700', marginLeft: 10 }}>Split Money</Text>
                            </LinearGradient>
                        </TouchableOpacity>

                    </View>
                </ScrollView>
                    :
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#000000', padding: 30, position: 'relative' }}>
                        {/* <LinearGradient
                        colors={['#E98607', '#9013D1']} style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}> */}
                        <Image source={require('../../../Assets/Images/checked.png')} style={{ height: 60, width: 60 }} />
                        {/* <IonIcon name='checkmark-circle' size={100} color={'#fff'} /> */}
                        <Text style={{ color: '#fff', fontSize: 22, fontFamily: 'Montserrat-Light', fontWeight: '700', marginLeft: 10, marginTop: 10 }}>Success</Text>
                        <Text style={{ color: '#fff', fontSize: 14, fontFamily: 'Montserrat-Light', fontWeight: '700', marginLeft: 10, marginVertical: 10 }}> Split request was sent to Successfully</Text>
                        <Text style={{ color: '#fff', fontSize: 30, fontFamily: 'Montserrat-Medium' }}>{pay.amount}</Text>
                        {/* </LinearGradient> */}
                        <TouchableOpacity style={{ position: 'absolute', bottom: 40, width: '100%', height: 55, marginTop: 40, borderRadius: 10 }}
                            onPress={() => { props.navigation.navigate('Home') }}>

                            <LinearGradient
                                colors={['#E98607', '#9013D1']} style={styles.loginLiner}>
                                <Text style={{ color: '#000000', fontSize: 18, fontFamily: 'Montserrat-SemiBold', marginLeft: 10 }}>Okay</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                }
            </ScrollView>
        </>
    );
}

const styles = StyleSheet.create({
    loginLiner: {
        width: '100%',
        height: 55,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        flexDirection: 'row'
    },
    money: {
        fontSize: 40,
        color: '#fff',
        fontFamily: 'Montserrat-Regular',
        // justifyContent: 'center',
        // alignItems: 'center',
        width: '100%',
        textAlign: 'center'
    },
    contactLabel: {
        fontSize: 12,
        color: 'grey',
        fontFamily: 'Montserrat-Bold',
        color: '#fff',

    },
    inputView: {
        borderBottomColor: '#1D1D1D',
        borderBottomWidth: 1,
        marginVertical: 10,
        color: '#fff',
    }
});

export default SplitMoney