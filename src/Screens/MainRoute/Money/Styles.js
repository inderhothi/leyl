import { StyleSheet } from 'react-native';
import Styles from '../Tabs/Styles';

const styles = StyleSheet.create({
    tabsContainerStyle: {
        backgroundColor: '#0593ff',
        width: '100%',
        borderRadius: 0,
    },
    tabStyle: {
        backgroundColor: '#1D1D1D',
        borderRadius: 0,
        borderColor: 'transparent',
        margin: 0,
        height: 40
    },
    tabTextStyle: {
        color: '#fff',
        fontSize: 16,
        borderRadius: 0,
    },

    activeTabStyle: {
        backgroundColor: '#1D1D1D',
        borderRadius: 0,
        borderBottomColor: '#C70237',
        borderWidth: 3,
        height: 40
    },
    activeTabTextStyle: {
        color: 'white',
        fontSize: 16,
        borderRadius: 0,
    },
})

export default Styles;