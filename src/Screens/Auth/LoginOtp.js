import { View, Text, Image, TouchableOpacity, TextInput } from 'react-native';
import React, { useState, useEffect } from 'react';
import LinearGradient from "react-native-linear-gradient";
import styles from './Styles';
import axios from 'axios';
import Constants from '../../../Helpers/constant';
import Spinner from '../../Styles/Spinner'
import Toaster from '../../Styles/Toaster'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { checkNumber } from '../../../serviceProvider/InputsNullChecker'
import OneSignal from 'react-native-onesignal';
import {
    CodeField,
    Cursor,
    useBlurOnFulfill,
    useClearByFocusCell,
} from 'react-native-confirmation-code-field';



const CELL_COUNT = 6;
const LoginOtp = (props) => {
    const [loading, setLoading] = useState(false);
    const [value, setValue] = useState('');
    const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
    const [property, getCellOnLayoutHandler] = useClearByFocusCell({
        value,
        setValue,
    });

    const login = async () => {
        const pushobj = await AsyncStorage.getItem('pushObj')
        const pushObj = JSON.parse(pushobj)
        console.log(value)
        if (!checkNumber(value) || value.length < 6) {
            return Toaster('invalid otp')
        }
        OneSignal.getDeviceState().then(async (data) => {
            console.log(data)
            await AsyncStorage.setItem('pushObj', JSON.stringify(data))
            setLoading(true);
            try {
                const d = {
                    phone: props?.route?.params?.phone,
                    otp: value,
                    push_token: data.pushToken,
                    player_id: data.userId
                }
                console.log(data)
                axios.post(`${Constants.baseUrl}login`, d).then((res) => {
                    setLoading(false);
                    if (res && res.data?.status == 200) {
                        AsyncStorage.setItem('regObj', JSON.stringify({ phone: props?.route?.params?.phone }))
                        const jsonValue = JSON.stringify(res.data)
                        AsyncStorage.setItem('userDetail', jsonValue)
                        // props.navigation.navigate('mainRoute');
                        props.navigation.navigate('Home');

                    } else {
                        Toaster(res.data.message);
                    }
                    console.log(res.data)
                }).catch(err => {
                    setLoading(false);
                    console.log(err)
                })
            } catch (err) {
                console.log(err)
            }
        })
    }


    const sendOtp = async () => {
        setLoading(true);
        try {
            axios.post(`${Constants.baseUrl}get-login-otp`, { phone: props?.route?.params?.phone }).then((res) => {
                setLoading(false);
                if (res && res.data.otp != undefined) {
                    Toaster('Otp sent successfully')
                } else {
                    Toaster(res.data.message);
                }
                console.log(res.data)
            }).catch(err => {
                setLoading(false);
                console.log(err)
            })
        } catch (err) {
            console.log(err)
        }


    }
    return (
        <View style={{ flex: 1, backgroundColor: '#000000' }}>
            <Spinner color={'#fff'} visible={loading} />
            {/* <LinearGradient
                colors={['#E98607', '#9013D1']} style={{ flex: 1 }}> */}
            <View style={styles.mainView}>
                <Image source={require('../../Assets/Images/logo-new.png')} style={{ height: 100, width: 100 }} />
                <View style={{ width: '100%', marginTop: 70 }}>
                    <Text style={styles.title}>We sent OTP code to verify your number</Text>
                    <View style={{ width: '100%', alignItems: 'center' }}>
                        <CodeField
                            ref={ref}
                            {...property}
                            // Use `caretHidden={false}` when users can't paste a text value, because context menu doesn't appear
                            value={value}
                            onChangeText={setValue}
                            cellCount={CELL_COUNT}
                            rootStyle={styles.codeFieldRoot2}
                            keyboardType="number-pad"
                            textContentType="oneTimeCode"
                            renderCell={({ index, symbol, isFocused }) => (
                                <Text
                                    key={index}
                                    style={[styles.cell2, isFocused && styles.focusCell]}
                                    onLayout={getCellOnLayoutHandler(index)}>
                                    {symbol || (isFocused ? <Cursor /> : null)}
                                </Text>
                            )}
                        />
                    </View>

                </View>
                {/* { value != undefined && value.length >= 4 && checkNumber(value) &&<Text style={{color:'#fff',marginTop:10}}>Invalid OTP</Text>} */}
                <TouchableOpacity style={{ width: '100%', height: 60, marginTop: 30, borderRadius: 10 }}
                    onPress={() => { login() }}>
                    <LinearGradient
                        colors={['#E98607', '#9013D1']} style={styles.loginLiner}>
                        <Text style={{ color: '#fff', fontSize: 22, fontFamily: 'Montserrat-Light', fontWeight: '700' }}>Login</Text>
                    </LinearGradient>
                </TouchableOpacity>
                <View style={{ flexDirection: 'row', marginTop: 20 }}>
                    <Text style={{ color: '#fff', fontFamily: 'Montserrat-Light', fontWeight: '700', margin: 2 }}>I didn't get the code?</Text>
                    <TouchableOpacity onPress={() => { sendOtp() }}>
                        <Text style={{ color: '#fff', fontFamily: 'Montserrat-Bold', fontWeight: '700', fontSize: 16 }}>Resend OTP</Text>
                    </TouchableOpacity>
                </View>
            </View>
            {/* </LinearGradient> */}
        </View>
    )
}

export default LoginOtp