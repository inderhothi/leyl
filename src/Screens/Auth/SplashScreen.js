import { Text, SafeAreaView, TouchableOpacity, Image } from 'react-native'
import React, { useEffect } from 'react'
import styles from './Styles'

const SplashScreen = (props) => {
    useEffect(() => {
        setTimeout(() => {
            props.navigation.replace('landingpage')
        }, 3000);
    }, [])
    return (
        <SafeAreaView style={[styles.container, { justifyContent: 'center', alignItems: 'center' }]}>
            <Image source={require('../../Assets/Images/logo-new.png')} style={{ height: 100, width: 100 }} />
        </SafeAreaView>
    )
}

export default SplashScreen