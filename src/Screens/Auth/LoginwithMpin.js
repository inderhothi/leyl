import { View, Text, Image, TouchableOpacity, TextInput, PermissionsAndroid } from 'react-native';
import React, { useState, useEffect } from 'react';
import LinearGradient from "react-native-linear-gradient";
import styles from './Styles';
import axios from 'axios';
import Constants from '../../../Helpers/constant';
import Spinner from '../../Styles/Spinner'
import Toaster from '../../Styles/Toaster'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { checkNumber } from '../../../serviceProvider/InputsNullChecker'
import OneSignal from 'react-native-onesignal';
import {
    CodeField,
    Cursor,
    useBlurOnFulfill,
    useClearByFocusCell,
    MaskSymbol,
    isLastFilledCell
} from 'react-native-confirmation-code-field';
import IonIcon from 'react-native-vector-icons/Ionicons';
// import * as Keychain from 'react-native-keychain'


const CELL_COUNT = 6;
const LoginwithMpin = (props) => {

    const [loading, setLoading] = useState(false);
    const [value, setValue] = useState('');
    const [phone, setPhone] = useState('');
    const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
    const [property, getCellOnLayoutHandler] = useClearByFocusCell({
        value,
        setValue,
    });

    // useEffect(() => {
    //     loginfaceId();
    //     return (() => {
    //         loginfaceId();
    //     })
    // }, [])


    // const loginfaceId = () => {

    //     Keychain.getSupportedBiometryType()
    //         .then(biometryType => {
    //             console.log(biometryType)
    //             // Success code
    //             if (biometryType === 'FaceID') {
    //                 console.log('FaceID is supported.');
    //             } else {
    //                 console.log('TouchID is supported.');
    //             }
    //         })
    //         .catch(error => {
    //             // Failure code
    //             console.log(error);
    //         });

    //     // Biometrics.authenticate('to demo this react-native component', optionalConfigObject)
    //     //     .then(success => {
    //     //         alert('Authenticated Successfully');
    //     //     })
    //     //     .catch(error => {
    //     //         alert('Authentication Failed');
    //     //     });
    //     // }
    // }

    const login = async () => {
        const pushobj = await AsyncStorage.getItem('pushObj')
        const pushObj = JSON.parse(pushobj)
        const obj = await AsyncStorage.getItem('regObj')
        var regObj = JSON.parse(obj)
        console.log(regObj)
        if (!checkNumber(value) || value.length != 6) {
            return Toaster('Enter 6 digit login PIN')
        }

        // if (!checkNumber(phone) || phone.length < 10) {
        //     return Toaster('The phone number does not valid.')
        // }
        OneSignal.getDeviceState().then(async (data) => {
            console.log(data)
            await AsyncStorage.setItem('pushObj', JSON.stringify(data))
            try {
                setLoading(true);
                const d = {
                    phone: regObj.phone,
                    // phone: 9904686607,
                    mpin: value,
                    push_token: data.pushToken,
                    player_id: data.userId
                }
                console.log(data)
                axios.post(`${Constants.baseUrl}login-with-mpin`, d).then(async (res) => {
                    setLoading(false);
                    console.log(res.data)
                    if (res && res.data?.status == 200) {
                        const jsonValue = JSON.stringify(res.data)
                        await AsyncStorage.setItem('userDetail', jsonValue)
                        // props.navigation.navigate('mainRoute');
                        props.navigation.replace('Home');

                    } else {
                        Toaster(res.data.message);
                    }
                    console.log(res.data)
                }).catch(err => {
                    setLoading(false);
                    console.log(err)
                })
            } catch (err) {
                setLoading(false);
                console.log(err)
            }
        })
    }

    const mask = (index, symbol, isFocused) => {
        let textChild = null;

        if (symbol) {
            textChild = (
                <MaskSymbol
                    // maskSymbol="❤️"
                    maskSymbol="*"
                    isLastFilledCell={isLastFilledCell({ index, value })}>
                    {symbol}
                </MaskSymbol>
            );
        } else if (isFocused) {
            textChild = <Cursor />;
        }
        return textChild
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#000000' }}>
            <Spinner color={'#fff'} visible={loading} />
            {/* <LinearGradient
                colors={['#E98607', '#9013D1']} style={{ flex: 1 }}> */}
            <View style={styles.mainView}>
                <Image source={require('../../Assets/Images/logo-new.png')} style={{ height: 100, width: 100 }} />
                <View style={{ width: '100%', marginTop: 20, alignItems: 'center' }}>
                    <Text style={{ fontSize: 14, color: 'grey', fontFamily: 'Montserrat-Light', fontWeight: '700', marginVertical: 10 }}>Login with Face ID</Text>
                    <Image source={require('../../Assets/Images/faceid.png')} style={{ height: 80, width: 80 }} onProgress={() => { loginfaceId() }} />
                    <Text style={{ fontSize: 14, color: 'grey', fontFamily: 'Montserrat-Light', fontWeight: '700', marginVertical: 10 }}>Or, Enter 6 Digit Login Pin</Text>
                    {/* <Text style={styles.title}>We sent OTP code to verify your number</Text> */}

                    {/* <Text style={{ fontSize: 14, color: '#fff', fontFamily: 'Montserrat-Light', fontWeight: '700', marginTop: 10 }}>MPin</Text> */}
                    <CodeField
                        ref={ref}
                        {...property}
                        // Use `caretHidden={false}` when users can't paste a text value, because context menu doesn't appear
                        value={value}
                        onChangeText={setValue}
                        cellCount={CELL_COUNT}
                        rootStyle={styles.codeFieldRoot2}
                        keyboardType="number-pad"
                        textContentType="oneTimeCode"
                        secureTextEntry={true}
                        renderCell={({ index, symbol, isFocused }) => (
                            <Text
                                key={index}
                                style={[styles.cell2, isFocused && styles.focusCell]}
                                onLayout={getCellOnLayoutHandler(index)}>
                                {/* {symbol || (isFocused ? <Cursor /> : '*')} */}
                                {mask(index, symbol, isFocused)}
                            </Text>
                        )}
                    />
                </View>
                {/* <View style={{ width: '60%', justifyContent: 'center' }}>
                    <View style={{ borderBottomColor: '#fff', borderBottomWidth: 1, marginTop: 30, position: 'relative' }}>
                        <Text style={{ fontSize: 14, color: '#fff', fontFamily: 'Montserrat-Light', fontWeight: '700' }}>Mobile Number</Text>
                        <TextInput placeholder='Enter Mobile Number' placeholderTextColor={'grey'} keyboardType='numeric' style={{ color: '#fff', fontSize: 16, fontFamily: 'Montserrat-Light', fontWeight: '700'}}
                            value={phone} onChangeText={(text) => { setPhone(text) }} />
                        {phone != undefined && phone.length >= 10 && checkNumber(phone) && <IonIcon name='checkmark-circle' size={16} color={'#fff'} style={{ position: 'absolute', right: 10, top: 40 }} />}
                    </View>
                </View> */}
                {/* { value != undefined && value.length >= 4 && checkNumber(value) &&<Text style={{color:'#fff',marginTop:10}}>Invalid OTP</Text>} */}
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 1, width: '100%', justifyContent: 'center', marginTop: 20 }}>
                        <TouchableOpacity
                            onPress={() => { props.navigation.navigate('forgotpassword') }}>
                            <Text style={{ color: '#fff', textAlign: 'left', fontFamily: 'Montserrat-Light', fontWeight: '700', opacity: 0.7, fontSize: 16 }}>Forget MPin?</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, width: '100%', justifyContent: 'flex-end', marginTop: 20 }}>

                        <TouchableOpacity
                            onPress={() => { props.navigation.navigate('login') }}>
                            <Text style={{ color: '#fff', textAlign: 'right', fontFamily: 'Montserrat-Light', fontWeight: '700', opacity: 0.7 }}>Login with Otp</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <TouchableOpacity style={{ width: '100%', height: 60, marginTop: 30, borderRadius: 10 }}
                    onPress={() => {
                        login();
                        // loginfaceId()
                    }}>
                    <LinearGradient
                        colors={['#E98607', '#9013D1']} style={styles.loginLiner}>
                        <Text style={{ color: '#fff', fontSize: 22, fontFamily: 'Montserrat-Light', fontWeight: '700' }}>Login</Text>
                    </LinearGradient>
                </TouchableOpacity>
                <View style={{ flexDirection: 'row', marginTop: 20 }}>
                    <Text style={{ color: '#fff', fontFamily: 'Montserrat-Light', fontWeight: '700', opacity: 0.7, margin: 2 }}>Don't have an acount?</Text>
                    <TouchableOpacity onPress={() => { props.navigation.navigate('stepOne') }}>
                        <Text style={{ color: '#fff', fontFamily: 'Montserrat-Bold', fontWeight: '700', fontSize: 16 }}>Create Account</Text>
                    </TouchableOpacity>
                </View>
                {/* <View style={{ flexDirection: 'row', marginTop: 20 }}>
                    <Text style={{ color: '#fff', fontFamily: 'Montserrat-Light', fontWeight: '700', margin: 2 }}>I didn't get the code?</Text>
                    <TouchableOpacity onPress={() => { sendOtp() }}>
                        <Text style={{ color: '#fff', fontFamily: 'Montserrat-Bold', fontWeight: '700', fontSize: 16 }}>Resend OTP</Text>
                    </TouchableOpacity>
                </View> */}
            </View>
            {/* </LinearGradient> */}
        </View>
    )
}

export default LoginwithMpin