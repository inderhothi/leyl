import { View, Text, Image, TouchableOpacity, TextInput } from 'react-native';
import React, { useState, useEffect } from 'react';
import LinearGradient from "react-native-linear-gradient";
import styles from './Styles';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import IonIcon from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import Constants from '../../../Helpers/constant';
import Spinner from '../../Styles/Spinner'
import Toaster from '../../Styles/Toaster'
import { checkForEmptyKeys, checkNumber, checkEmail } from '../../../serviceProvider/InputsNullChecker'
import { getContactsByPhoneNumber } from 'react-native-contacts';


const Login = (props) => {

  const [loading, setLoading] = useState(false);
  const [filedCheck, setfiledCheck] = useState([]);
  const [showMobile, setShowMobile] = useState(true);
  const [regObj, setRegObj] = useState({
    // phone: '',
    email: 'ajyshrma93@gmail.com',
    password: 'Admin@123',
    // email: '',
    // password: '',
  });
  const [phone, setPhone] = useState('')

  const login = async () => {
    let { errorString, anyEmptyInputs } = checkForEmptyKeys(regObj);
    setfiledCheck(anyEmptyInputs)
    console.log(anyEmptyInputs)
    if (anyEmptyInputs.length == 0) {

      if (regObj.password.length < 8) {
        return Toaster('The password must be at least 8 characters.')
      }

      if (!checkEmail(regObj.email)) {
        return Toaster('The email does not valid.')
      }


      setLoading(true);
      try {
        axios.post(`${Constants.baseUrl}login-with-password`, regObj).then((res) => {
          setLoading(false);
          if (res.data.status == 200) {
            const jsonValue = JSON.stringify(res.data)
            AsyncStorage.setItem('userDetail', jsonValue)
            // props.navigation.navigate('mainRoute');
            props.navigation.navigate('Home');
            setRegObj({});
          } else {
            Toaster('The email address or password you entered is incorrect')
          }

          console.log(res.data)
        }).catch(err => {
          setLoading(false);
          console.log(err)
        })
      } catch (err) {
        console.log(err)
      }
    } else {
      Toaster(errorString)
    }

  }

  const sendOtp = async () => {
    if (!checkNumber(phone) || phone.length < 10) {
      return Toaster('The phone number does not valid.')
    }
    setLoading(true);
    try {
      axios.post(`${Constants.baseUrl}get-login-otp`, { phone }).then((res) => {
        setLoading(false);
        if (res && res.data.otp != undefined) {

          props.navigation.navigate('loginOtp', { phone });
          Toaster('Otp sent successfully')
          setRegObj({});

        } else {
          Toaster(res.data.message);
        }

        console.log(res.data)
      }).catch(err => {
        setLoading(false);
        console.log(err)
      })
    } catch (err) {
      console.log(err)
    }


  }

  return (
    <View style={{ flex: 1, backgroundColor: '#000000' }}>
      <Spinner color={'#fff'} visible={loading} />
      {/* <LinearGradient  
        colors={['#E98607', '#9013D1']} style={{ flex: 1 }}> */}
      <View style={styles.mainView}>
        <Image source={require('../../Assets/Images/logo-new.png')} style={{ height: 100, width: 100 }} />

        {!showMobile && <View style={{ width: '100%', justifyContent: 'flex-start' }}>
          <View style={{ borderBottomColor: '#B03C88', borderBottomWidth: 1, marginTop: 50, position: 'relative' }}>
            <Text style={{ fontSize: 14, color: '#fff', fontFamily: 'Montserrat-Light', fontWeight: '700' }}>Email</Text>
            <TextInput style={{ color: '#fff', fontSize: 18, fontFamily: 'Montserrat-Light', fontWeight: '700' }}
              value={regObj.email} onChangeText={(text) => { setRegObj({ ...regObj, email: text }) }} />
            {regObj.email != undefined && regObj.email.length > 0 && checkEmail(regObj.email) && <IonIcon name='checkmark-circle' size={16} color={'#fff'} style={{ position: 'absolute', right: 10, top: 40 }} />}
          </View>

          <View style={{ borderBottomColor: '#BE4F68', borderBottomWidth: 1, marginTop: 20, position: 'relative' }}>
            <Text style={{ fontSize: 14, color: '#fff', fontFamily: 'Montserrat-Light', fontWeight: '700' }}>Password</Text>
            <TextInput secureTextEntry={true} style={{ color: '#fff', fontSize: 18, fontFamily: 'Montserrat-Light', fontWeight: '700' }}
              value={regObj.password} onChangeText={(text) => { setRegObj({ ...regObj, password: text }) }} />
            {regObj.password != undefined && regObj.password.length >= 8 && <IonIcon name='checkmark-circle' size={16} color={'#fff'} style={{ position: 'absolute', right: 10, top: 40 }} />}
          </View>
        </View>
        }

        {showMobile && <View style={{ width: '100%', justifyContent: 'flex-start' }}>
          <View style={{ borderBottomColor: '#B03C88', borderBottomWidth: 1, marginTop: 50, position: 'relative' }}>
            <Text style={{ fontSize: 14, color: '#fff', fontFamily: 'Montserrat-Light', fontWeight: '700' }}>Mobile Number</Text>
            <TextInput placeholder='Enter Mobile Number' placeholderTextColor={'lightgrey'} keyboardType='numeric' style={{ color: '#fff', fontSize: 16, fontFamily: 'Montserrat-Light', fontWeight: '700' }}
              value={phone} onChangeText={(text) => { setPhone(text) }} />
            {phone != undefined && phone.length >= 10 && checkNumber(phone) && <IonIcon name='checkmark-circle' size={16} color={'#fff'} style={{ position: 'absolute', right: 10, top: 40 }} />}
          </View>
        </View>
        }


        <View style={{ flexDirection: 'row' }}>
          {!showMobile && <View style={{ flex: 1, width: '100%', justifyContent: 'flex-start', marginTop: 20 }}>
            <TouchableOpacity
              onPress={() => { props.navigation.navigate('forgotpassword') }}>
              <Text style={{ color: '#fff', textAlign: 'left', fontFamily: 'Montserrat-Light', fontWeight: '700', opacity: 0.7 }}>Forget Password?</Text>
            </TouchableOpacity>
          </View>}

          <View style={{ flex: 1, width: '100%', justifyContent: 'center', marginTop: 20 }}>
            <TouchableOpacity
              onPress={() => { props.navigation.navigate('loginMpin') }}>
              <Text style={{ color: '#fff', textAlign: !showMobile ? 'center' : 'left', fontFamily: 'Montserrat-Light', fontWeight: '700', opacity: 0.7 }}>Login with MPin</Text>
            </TouchableOpacity>
          </View>
          {/* 
          <View style={{ flex: 1, width: '100%', justifyContent: 'flex-end', marginTop: 20 }}>
            {!showMobile ? <TouchableOpacity
              onPress={() => { setShowMobile(true) }}>
              <Text style={{ color: '#fff', textAlign: 'right', fontFamily: 'Montserrat-Light', fontWeight: '700', opacity: 0.7 }}>Login with OTP</Text>
            </TouchableOpacity>
              :
              <TouchableOpacity
                onPress={() => { setShowMobile(false) }}>
                <Text style={{ color: '#fff', textAlign: 'right', fontFamily: 'Montserrat-Light', fontWeight: '700', opacity: 0.7 }}>Login with password</Text>
              </TouchableOpacity>
            }
          </View> */}
        </View>

        {!showMobile ? <TouchableOpacity style={{ width: '100%', height: 60, marginTop: 20, borderRadius: 10 }}
          onPress={() => { login() }}>
          <LinearGradient
            colors={['#E98607', '#9013D1']} style={styles.loginLiner}>
            <Text style={{ color: '#fff', fontSize: 22, fontFamily: 'Montserrat-Light', fontWeight: '700' }}>Login</Text>
          </LinearGradient>
        </TouchableOpacity>
          :
          <TouchableOpacity style={{ width: '100%', height: 60, marginTop: 20, borderRadius: 10 }}
            onPress={() => { sendOtp() }}>
            <LinearGradient
              colors={['#E98607', '#9013D1']} style={styles.loginLiner}>
              <Text style={{ color: '#fff', fontSize: 22, fontFamily: 'Montserrat-Light', fontWeight: '700' }}>Login With OTP</Text>
            </LinearGradient>
          </TouchableOpacity>
        }

        <View style={{ flexDirection: 'row', marginTop: 20 }}>
          <Text style={{ color: '#fff', fontFamily: 'Montserrat-Light', fontWeight: '700', opacity: 0.7, margin: 2 }}>Don't have an acount?</Text>
          <TouchableOpacity onPress={() => { props.navigation.navigate('stepOne') }}>
            <Text style={{ color: '#fff', fontFamily: 'Montserrat-Bold', fontWeight: '700', fontSize: 16 }}>Create Account</Text>
          </TouchableOpacity>
        </View>
      </View>
      {/* </LinearGradient> */}
    </View>
  );
};

export default Login;
