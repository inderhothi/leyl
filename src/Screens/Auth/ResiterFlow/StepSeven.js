import { Text, SafeAreaView, TouchableOpacity, Image } from 'react-native'
import React from 'react'
import Styles from './Styles'


const StepSeven = (props) => {
    return (
        <SafeAreaView style={[Styles.container, { justifyContent: 'center', alignItems: 'center' }]}>
            <Image source={require('../../../Assets/Images/logo-new.png')} style={{ height: 100, width: 100 }} />
            <Text style={{ color: '#fff', fontSize: 20, fontFamily: 'Montserrat-SemiBold', marginTop: 80, marginLeft: 10 }}>Registration successful!  </Text>
            <Text style={{ color: '#fff', fontSize: 16, fontFamily: 'Montserrat-SemiBold', marginTop: 10, marginLeft: 10, opacity: 0.8 }}>Discover your digital wallet  </Text>
            <TouchableOpacity style={[Styles.regbtn2, { marginTop: 150 }]}
                onPress={() => { props.navigation.navigate('stepEight') }}>
                <Text style={{ color: '#000000', fontSize: 18, fontWeight: '700', fontFamily: 'Montserrat-Light' }}>Get Started</Text>
            </TouchableOpacity>
        </SafeAreaView>
    )
}

export default StepSeven