import { View, Text, SafeAreaView, TouchableOpacity, TextInput, Image } from 'react-native'
import React, { useState } from 'react'
import Styles from './Styles'
import LinearGradient from "react-native-linear-gradient";
import { Avatar, RadioButton } from 'react-native-paper';
import Toaster from '../../../Styles/Toaster'
import { checkForEmptyKeys, checkNumber, checkEmail } from '../../../../serviceProvider/InputsNullChecker'
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import Constants from '../../../../Helpers/constant';
import Spinner from '../../../Styles/Spinner'

const StepSix = (props) => {
    const [loading, setLoading] = useState(false);
    const contine = async () => {
        const obj = await AsyncStorage.getItem('regObj')

        const regObj = JSON.parse(obj)
        console.log(regObj)


        setLoading(true);
        try {
            regObj.email = '';
            regObj.name = '';
            regObj.otp = await AsyncStorage.getItem('otp');
            regObj.password = '';
            console.log(regObj)
            axios.post(`${Constants.baseUrl}register`, regObj).then((res) => {
                console.log(res.data)
                setLoading(false);
                if (res && res.data?.status == 200) {

                    // AsyncStorage.removeItem('regObj');
                    props.navigation.navigate('stepSeven')
                    // props.navigation.navigate('loginMpin');
                    Toaster('Registered successfully')
                    //   setRegObj({});
                } else {

                    Toaster(res.data.message);
                }

                console.log(res.data)
            }).catch(err => {
                setLoading(false);
                console.log(err)
            })
        } catch (err) {
            console.log(err)
        }



        // regObj.mpin = pin.newPin

        // const jsonValue = JSON.stringify(regObj)
        // await AsyncStorage.setItem('regObj', jsonValue)

    }
    return (
        <SafeAreaView style={Styles.container}>
            <Spinner color={'#fff'} visible={loading} />
            <View style={{ flex: 1, position: 'relative' }}>
                <Text style={{ color: '#fff', fontSize: 12, marginTop: 30, marginBottom: 5, fontFamily: 'Montserrat-Regular' }}>Step 6/8</Text>
                <View
                    style={{ height: 7, borderRadius: 5, backgroundColor: '#fff' }}
                    onPress={() => { }}
                >

                    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        colors={['#9013D1', '#E98607']} style={{ width: '75%', height: 7, justifyContent: 'center', borderRadius: 5 }}>
                    </LinearGradient>
                </View>
                <View>
                    <Text style={{ color: '#fff', fontSize: 16, marginTop: 30, marginBottom: 10, fontFamily: 'Montserrat-Regular' }}>Select Account</Text>

                    <Text style={{ color: '#fff', fontSize: 12, fontFamily: 'Montserrat-Regular', marginTop: 5 }}>Which account do you want for LEYL use?</Text>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                        <Image source={require('../../../Assets/Images/checked.png')} style={{ height: 35, width: 35 }} />
                        <Text style={{ color: '#fff', fontSize: 20, fontFamily: 'Montserrat-SemiBold', marginTop: 5, marginLeft: 10 }}>CHXXXXXXXXXXXXXXX4498
                            Automobile</Text>
                    </View>

                </View>
                <View style={{ position: 'absolute', bottom: '5%', width: '100%' }}>
                    <TouchableOpacity style={Styles.regbtn2}
                        onPress={() => { contine(); }}>
                        <Text style={{ color: '#000000', fontSize: 18, fontWeight: '700', fontFamily: 'Montserrat-Light' }}>Continue</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={Styles.regbtn}
                        onPress={() => { props.navigation.goBack() }}>
                        <Text style={{ color: '#fff', fontSize: 18, fontWeight: '700', fontFamily: 'Montserrat-Light' }}>Back</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </SafeAreaView>
    )
}

export default StepSix