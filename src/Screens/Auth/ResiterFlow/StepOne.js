import { View, Text, SafeAreaView, TouchableOpacity } from 'react-native'
import React, { useState } from 'react'
import Styles from './Styles'
import LinearGradient from "react-native-linear-gradient";
import { Avatar, RadioButton } from 'react-native-paper';

const StepOne = (props) => {
  const [registered, setRegisterd] = useState('yes')
  return (
    <SafeAreaView style={Styles.container}>
      <View style={{ flex: 1,position:'relative' }}>
        <Text style={{ color: '#fff', fontSize: 12, marginTop: 30, marginBottom: 5, fontFamily: 'Montserrat-Regular' }}>Step 1/8</Text>
        <View
          style={{ height: 7, borderRadius: 5, backgroundColor: '#fff' }}
          onPress={() => { }}
        >

          <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
            colors={['#9013D1', '#E98607']} style={{ width: '12.5%', height: 7, justifyContent: 'center', borderRadius: 5 }}>
          </LinearGradient>
        </View>
        <View>
          <Text style={{ color: '#fff', fontSize: 16, marginTop: 30, marginBottom: 10, fontFamily: 'Montserrat-Regular' }}>Use this LEYL app first time?</Text>
          <Text style={{ color: '#fff', fontSize: 12, fontFamily: 'Montserrat-Regular' }}>Choose «YES» if you die Valiant LEYL Install app for the first time and reboot want to register.
            Important: Even if you another LEYL app (e.g. LEYL prepaid app) you have to register again to register Select «NO» when using this app Derents are
            registered and for example one perform a device change.</Text>
          <View style={{ marginTop: 30 }}>
            <RadioButton.Group
              onValueChange={newValue => { setRegisterd(newValue) }}
              value={registered}
            >
              <View style={{ flexDirection: 'row', height: 30 }}>
                <RadioButton value="yes" color='#9013D1' uncheckedColor='#fff' />
                <View style={Styles.radioTextView}>
                  <Text style={{ color: '#fff', fontSize: 12, fontFamily: 'Montserrat-Regular', marginTop: 8, marginLeft: 5 }}>YES, I am not registered yet </Text>
                </View>
              </View>

              <View style={{ flexDirection: 'row', height: 30, marginTop: 20 }}>
                <RadioButton value="no" color='#9013D1' uncheckedColor='#fff' />
                <View style={Styles.radioTextView}>
                  <Text style={{ color: '#fff', fontSize: 12, fontFamily: 'Montserrat-Regular', marginTop: 8, marginLeft: 5 }}>NO, I already use this app</Text>
                </View>
              </View>
            </RadioButton.Group>
          </View>
        </View>
        <View style={{ position: 'absolute', bottom: '5%', width: '100%' }}>
          <TouchableOpacity style={Styles.regbtn2}
            onPress={() => { props.navigation.navigate(registered == 'yes'? 'stepTwo' :'loginMpin') }}>
            <Text style={{ color: '#000000', fontSize: 18, fontWeight: '700', fontFamily: 'Montserrat-Light' }}>Continue</Text>
          </TouchableOpacity>

          <TouchableOpacity style={Styles.regbtn}
            onPress={() => { props.navigation.goBack() }}>
            <Text style={{ color: '#fff', fontSize: 18, fontWeight: '700', fontFamily: 'Montserrat-Light' }}>Back</Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  )
}

export default StepOne

