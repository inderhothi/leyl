import { Text, SafeAreaView, TouchableOpacity, Image } from 'react-native'
import React from 'react'
import Styles from './Styles'

const StepEight = (props) => {
    return (
        <SafeAreaView style={[Styles.container, { justifyContent: 'center', alignItems: 'center' }]}>
            <Image source={require('../../../Assets/Images/logo-new.png')} style={{ height: 100, width: 100 }} />
            <Text style={{ color: '#fff', fontSize: 20, fontFamily: 'Montserrat-SemiBold', marginTop: 80, marginLeft: 10, textAlign: 'center' }}>
                Pay and send quickly and easily with the LEYL app</Text>
            <TouchableOpacity style={[Styles.regbtn2, { marginTop: 150 }]}
                onPress={() => { props.navigation.navigate('loginMpin') }}>
                <Text style={{ color: '#000000', fontSize: 18, fontWeight: '700', fontFamily: 'Montserrat-Light' }}>Pay Now</Text>
            </TouchableOpacity>
        </SafeAreaView>
    )
}

export default StepEight