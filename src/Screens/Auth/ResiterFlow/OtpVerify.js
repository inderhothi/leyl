import { View, Text, SafeAreaView, TouchableOpacity, TextInput } from 'react-native'
import React, { useState } from 'react'
import Styles from './Styles'
import LinearGradient from "react-native-linear-gradient";
import { Avatar, RadioButton } from 'react-native-paper';
import Toaster from '../../../Styles/Toaster'
import { checkForEmptyKeys, checkNumber, checkEmail } from '../../../../serviceProvider/InputsNullChecker'
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import Constants from '../../../../Helpers/constant';
import Spinner from '../../../Styles/Spinner'

const OtpVerify = () => {
    const [phone, setPhone] = useState('')
    const [loading, setLoading] = useState(false);

    const contine = async () => {
        if (!checkNumber(phone) || phone.length < 10) {
            return Toaster('The phone number does not valid.')
        }

        try {
            setLoading(true);
            axios.post(`${Constants.baseUrl}verify-user`, { phone }).then((res) => {
                setLoading(false);
                if (res && res.data.otp != undefined) {
                    const jsonValue = JSON.stringify({ phone })
                    AsyncStorage.setItem('regObj', jsonValue)
                    props.navigation.navigate('stepThree', { phone })
                    Toaster('Otp sent successfully')
                    // setRegObj({});

                } else {
                    if (res.data.phone != undefined) {
                        Toaster(res.data.phone[0]);
                    }
                    if (res.data.phone != undefined) {
                        Toaster(res.data.email[0]);
                    }
                }

                console.log(res.data)
            }).catch(err => {
                setLoading(false);
                console.log(err)
            })
        } catch (err) {
            console.log(err)
        }


    }



    return (
        <SafeAreaView style={Styles.container}>
            <Spinner color={'#fff'} visible={loading} />
            <View style={{ flex: 1, position: 'relative' }}>
                <Text style={{ color: '#fff', fontSize: 12, marginTop: 30, marginBottom: 5, fontFamily: 'Montserrat-Regular' }}>Step 2/8</Text>
                <View
                    style={{ height: 7, borderRadius: 5, backgroundColor: '#fff' }}
                    onPress={() => { }}
                >

                    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        colors={['#9013D1', '#E98607']} style={{ width: '25%', height: 7, justifyContent: 'center', borderRadius: 5 }}>
                    </LinearGradient>
                </View>
                <View>
                    <Text style={{ color: '#fff', fontSize: 16, marginTop: 30, marginBottom: 10, fontFamily: 'Montserrat-Regular' }}>Here we go</Text>
                    <TextInput
                        value={phone}
                        onChangeText={(text) => { setPhone(text) }}
                        style={{ color: '#fff', borderBottomColor: '#850631', borderBottomWidth: 1 }}
                        placeholder='Please Enter your mobile number'
                        placeholderTextColor={'#fff'}
                        keyboardType='numeric'
                    />

                    <Text style={{ color: '#fff', fontSize: 12, fontFamily: 'Montserrat-Regular', marginTop: 20 }}>By entering my cell phone number, I accept the
                        terms of use and the privacy policy.</Text>

                </View>
                <View style={{ position: 'absolute', bottom: '5%', width: '100%' }}>
                    <TouchableOpacity style={Styles.regbtn2}
                        onPress={() => { contine(); }}>
                        <Text style={{ color: '#000000', fontSize: 18, fontWeight: '700', fontFamily: 'Montserrat-Light' }}>Continue</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={Styles.regbtn}
                        onPress={() => { props.navigation.goBack() }}>
                        <Text style={{ color: '#fff', fontSize: 18, fontWeight: '700', fontFamily: 'Montserrat-Light' }}>Back</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </SafeAreaView>
    )
}

export default OtpVerify