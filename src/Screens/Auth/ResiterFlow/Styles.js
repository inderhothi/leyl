import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({

    container:{
        flex:1,
        backgroundColor:'#000000',
        padding:30
    },
    radioTextView: {
        marginLeft: 10,
        justifyContent: 'center',
        // alignItems: 'flex-start'
    },
    radioText: {
        color: '#fff',
        fontSize: 14,
        fontWeight: '500'
    },

    regbtn: {
        width: '100%',
        height: 55,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        borderColor: '#fff',
        borderWidth: 3,
        borderRadius: 10
      },

      regbtn2: {
        width: '100%',
        height: 55,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        borderWidth: 3,
        borderRadius: 10,
        backgroundColor:'#fff',
        marginTop:100
      },
    

});

export default styles;