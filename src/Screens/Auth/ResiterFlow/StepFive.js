import { View, Text, SafeAreaView, TouchableOpacity, TextInput, Image } from 'react-native'
import React, { useState } from 'react'
import Styles from './Styles'
import LinearGradient from "react-native-linear-gradient";
import { Avatar, RadioButton } from 'react-native-paper';

const StepFive = (props) => {
    const [account, setAccount] = useState('')
  return (
    <SafeAreaView style={Styles.container}>
    <View style={{ flex: 1, position: 'relative' }}>
    <Text style={{ color: '#fff', fontSize: 12, marginTop: 30, marginBottom: 5, fontFamily: 'Montserrat-Regular' }}>Step 5/8</Text>
    <View
        style={{ height: 7, borderRadius: 5, backgroundColor: '#fff' }}
        onPress={() => { }}
    >

        <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
            colors={['#9013D1', '#E98607']} style={{ width: '62.5%', height: 7, justifyContent: 'center', borderRadius: 5 }}>
        </LinearGradient>
    </View>
    <View>
        <Text style={{ color: '#fff', fontSize: 16, marginTop: 30, marginBottom: 10, fontFamily: 'Montserrat-Regular' }}>Authentication </Text>

        <Text style={{ color: '#fff', fontSize: 12, fontFamily: 'Montserrat-Regular', marginTop: 5 }}>Please log in with your e-banking access code.  
        If you are not yet registered for Valiant LEYL are registered, you will receive in the next Send a letter with a QR code. Scan this for registration to shoot down</Text>
        <TextInput
        value={account}
        onChangeText={(text)=>{setAccount(text)}}
            style={{ color: '#fff', borderBottomColor: '#5D4050', borderBottomWidth: 1, marginTop: 20,fontFamily: 'Montserrat-Regular' }}
            placeholder='Enter E-Banking contract number'
            placeholderTextColor={'#fff'}
            keyboardType='numeric'
        />

        <TextInput
            style={{ color: '#fff', borderBottomColor: '#CC6149', borderBottomWidth: 1,fontFamily: 'Montserrat-Regular' }}
            placeholder='Enter Password'
            placeholderTextColor={'#fff'}
            keyboardType='numeric'
        />
    </View>
    <View style={{ position: 'absolute', bottom: '5%', width: '100%' }}>
        <TouchableOpacity style={Styles.regbtn2}
            onPress={() => { props.navigation.navigate('stepSix',{account}) }}>
            <Text style={{ color: '#000000', fontSize: 18, fontWeight: '700', fontFamily: 'Montserrat-Light' }}>Continue</Text>
        </TouchableOpacity>

        <TouchableOpacity style={Styles.regbtn}
            onPress={() => { props.navigation.goBack() }}>
            <Text style={{ color: '#fff', fontSize: 18, fontWeight: '700', fontFamily: 'Montserrat-Light' }}>Back</Text>
        </TouchableOpacity>
    </View>
</View>
</SafeAreaView>
  )
}

export default StepFive