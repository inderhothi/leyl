import { View, Text, SafeAreaView, TouchableOpacity, TextInput, Image } from 'react-native'
import React, { useState } from 'react'
import Styles from './Styles'
import LinearGradient from "react-native-linear-gradient";
import { Avatar, RadioButton } from 'react-native-paper';
import Toaster from '../../../Styles/Toaster'
import { checkForEmptyKeys, checkNumber, checkEmail } from '../../../../serviceProvider/InputsNullChecker'
import AsyncStorage from '@react-native-async-storage/async-storage';

const StepFour = (props) => {
    const [pin, setpin] = useState({
        newPin: '',
        comfirmPin: ''
    })

    const contine = async () => {
        const obj = await AsyncStorage.getItem('regObj')
        const regObj = JSON.parse(obj)
        if (!checkNumber(pin.newPin) || pin.newPin.length < 6) {
            return Toaster('The Mpin does not valid.')
        }
        if (!checkNumber(pin.comfirmPin) || pin.comfirmPin.length < 6) {
            return Toaster('The ConfirmMPin does not valid.')
        }

        if (pin.newPin != pin.comfirmPin) {
            return Toaster('The password does not match.')
        }

        regObj.mpin = pin.newPin

        const jsonValue = JSON.stringify(regObj)
        await AsyncStorage.setItem('regObj', jsonValue)
        props.navigation.navigate('stepFive')
    }

    return (
        <SafeAreaView style={Styles.container}>
            <View style={{ flex: 1, position: 'relative' }}>
                <Text style={{ color: '#fff', fontSize: 12, marginTop: 30, marginBottom: 5, fontFamily: 'Montserrat-Regular' }}>Step 4/8</Text>
                <View
                    style={{ height: 7, borderRadius: 5, backgroundColor: '#fff' }}
                    onPress={() => { }}
                >

                    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        colors={['#9013D1', '#E98607']} style={{ width: '50%', height: 7, justifyContent: 'center', borderRadius: 5 }}>
                    </LinearGradient>
                </View>
                <View>
                    <Text style={{ color: '#fff', fontSize: 16, marginTop: 30, marginBottom: 10, fontFamily: 'Montserrat-Regular' }}>Better safe than sorry </Text>

                    <Text style={{ color: '#fff', fontSize: 12, fontFamily: 'Montserrat-Regular', marginTop: 5 }}>Protect your digital wallet with a six-digit PIN.</Text>
                    <TextInput
                        value={pin.newPin}
                        onChangeText={(text) => { setpin({ ...pin, newPin: text }) }}
                        style={{ color: '#fff', borderBottomColor: '#850631', borderBottomWidth: 1, marginTop: 20 }}
                        placeholder='Define PIN (6 digits)]'
                        placeholderTextColor={'#fff'}
                        keyboardType='numeric'
                    />

                    <TextInput
                        value={pin.comfirmPin}
                        onChangeText={(text) => { setpin({ ...pin, comfirmPin: text }) }}
                        style={{ color: '#fff', borderBottomColor: '#850631', borderBottomWidth: 1, }}
                        placeholder='Confirm PiN ( 6 digits )] '
                        placeholderTextColor={'#fff'}
                        keyboardType='numeric'
                    />
                    <TouchableOpacity style={{ width: '100%', justifyContent: 'center', alignItems: 'center', marginTop: 50 }}>
                        <Image source={require('../../../Assets/Images/Touch.png')} style={{ height: 80, width: 80 }} />
                    </TouchableOpacity>

                </View>
                <View style={{ position: 'absolute', bottom: '5%', width: '100%' }}>
                    <TouchableOpacity style={Styles.regbtn2}
                        onPress={() => { contine() }}>
                        <Text style={{ color: '#000000', fontSize: 18, fontWeight: '700', fontFamily: 'Montserrat-Light' }}>Continue</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={Styles.regbtn}
                        onPress={() => { props.navigation.goBack() }}>
                        <Text style={{ color: '#fff', fontSize: 18, fontWeight: '700', fontFamily: 'Montserrat-Light' }}>Back</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </SafeAreaView>
    )
}

export default StepFour