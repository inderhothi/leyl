import { View, Text, Image, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import React, { useEffect, useState } from 'react';
import LinearGradient from "react-native-linear-gradient";
import styles from './Styles';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import IonIcon from 'react-native-vector-icons/Ionicons';
import axios from 'axios';
import Constants from '../../../Helpers/constant';
import Spinner from '../../Styles/Spinner'
import Toaster from '../../Styles/Toaster'
import { checkForEmptyKeys, checkNumber, checkEmail } from '../../../serviceProvider/InputsNullChecker'
import AsyncStorage from '@react-native-async-storage/async-storage';

const Register = (props) => {
  const [loading, setLoading] = useState(false);
  const [filedCheck, setfiledCheck] = useState([]);
  const [regObj, setRegObj] = useState({
    name: '',
    email: '',
    phone: '',
    password: '',
    comfirmPassord: '',
    mpin: ''
  });

  useEffect(() => {
  }, [])

  const checkValition = () => {
    let { errorString, anyEmptyInputs } = checkForEmptyKeys(regObj);
    setfiledCheck(anyEmptyInputs)
  }


  const register = async () => {
    let { errorString, anyEmptyInputs } = checkForEmptyKeys(regObj);
    setfiledCheck(anyEmptyInputs)
    console.log(anyEmptyInputs)
    if (anyEmptyInputs.length == 0) {

      if (regObj.password.length < 8) {
        return Toaster('The password must be at least 8 characters.')
      }

      if (regObj.mpin.length != 4) {
        return Toaster('The Mpin must be at least 4 characters.')
      }

      if (regObj.password != regObj.comfirmPassord) {
        return Toaster('The password does not match.')
      }

      if (!checkNumber(regObj.phone) || regObj.phone.length < 10) {
        return Toaster('The phone number does not valid.')
      }

      if (!checkEmail(regObj.email)) {
        return Toaster('The email does not valid.')
      }

      const jsonValue = JSON.stringify(regObj)
      await AsyncStorage.setItem('regObj', jsonValue)
      setLoading(true);
      try {
        axios.post(`${Constants.baseUrl}verify-user`, regObj).then((res) => {
          setLoading(false);
          if (res && res.data.otp != undefined) {

            props.navigation.navigate('otpScreen');
            Toaster('Otp sent successfully')
            setRegObj({});

          } else {
            if (res.data.phone != undefined) {
              Toaster(res.data.phone[0]);
            }
            if (res.data.phone != undefined) {
              Toaster(res.data.email[0]);
            }
          }

          console.log(res.data)
        }).catch(err => {
          setLoading(false);
          console.log(err)
        })
      } catch (err) {
        console.log(err)
      }
    } else {
      Toaster(errorString)
    }

  }
  return (
    <View style={{ flex: 1, backgroundColor: '#000000' }}>
      <Spinner color={'#fff'} visible={loading} />
      {/* <LinearGradient
        colors={['#E98607', '#9013D1']} style={{ flex: 1 }}> */}
      {/* <View style={styles.mainView}> */}
      <ScrollView contentContainerStyle={{ padding: 30, alignItems: 'center' }}>
        <Image source={require('../../Assets/Images/logo-new.png')} style={{ height: 100, width: 100 }} />

        <View style={{ width: '100%', justifyContent: 'flex-start' }}>
          <View style={{ borderBottomColor: '#BE4F68', borderBottomWidth: 1, marginTop: 30, position: 'relative' }}>
            <Text style={{ fontSize: 14, color: '#fff', opacity: 0.7, fontFamily: 'Montserrat-Light', fontWeight: '700' }}>Name</Text>
            <TextInput style={{ color: '#fff', fontSize: 16, fontFamily: 'Montserrat-Light', fontWeight: '700' }}
              value={regObj.name} onChangeText={(text) => { setRegObj({ ...regObj, name: text }); checkValition() }} />
            {regObj.name != undefined && regObj.name.length > 0 && <IonIcon name='checkmark-circle' size={16} color={'#fff'} style={{ position: 'absolute', right: 10, top: 40 }} />}
          </View>

          <View style={{ borderBottomColor: '#B03C88', borderBottomWidth: 1, marginTop: 20, position: 'relative' }}>
            <Text style={{ fontSize: 14, color: '#fff', opacity: 0.7, fontFamily: 'Montserrat-Light', fontWeight: '700' }}>Email</Text>
            <TextInput style={{ color: '#fff', fontSize: 16, fontFamily: 'Montserrat-Light', fontWeight: '700' }}
              value={regObj.email} onChangeText={(text) => { setRegObj({ ...regObj, email: text }); checkValition() }} />
            {regObj.email != undefined && regObj.email.length > 0 && checkEmail(regObj.email) && <IonIcon name='checkmark-circle' size={16} color={'#fff'} style={{ position: 'absolute', right: 10, top: 40 }} />}
          </View>

          <View style={{ borderBottomColor: '#B03C88', borderBottomWidth: 1, marginTop: 20, position: 'relative' }}>
            <Text style={{ fontSize: 14, color: '#fff', opacity: 0.7, fontFamily: 'Montserrat-Light', fontWeight: '700' }}>Phone</Text>
            <TextInput style={{ color: '#fff', fontSize: 16, fontFamily: 'Montserrat-Light', fontWeight: '700' }} keyboardType='numeric'
              value={regObj.phone} onChangeText={(text) => { setRegObj({ ...regObj, phone: text }); checkValition() }} />
            {regObj.phone != undefined && regObj.phone.length >= 10 && checkNumber(regObj.phone) && <IonIcon name='checkmark-circle' size={16} color={'#fff'} style={{ position: 'absolute', right: 10, top: 40 }} />}
          </View>

          <View style={{ borderBottomColor: '#B03C88', borderBottomWidth: 1, marginTop: 20, position: 'relative' }}>
            <Text style={{ fontSize: 14, color: '#fff', opacity: 0.7, fontFamily: 'Montserrat-Light', fontWeight: '700' }}>MPin</Text>
            <TextInput secureTextEntry={true} style={{ color: '#fff', fontSize: 16, fontFamily: 'Montserrat-Light', fontWeight: '700' }}
              value={regObj.mpin} onChangeText={(text) => { setRegObj({ ...regObj, mpin: text }); checkValition() }} />
            {regObj.mpin != undefined && regObj.mpin.length == 4 && <IonIcon name='checkmark-circle' size={16} color={'#fff'} style={{ position: 'absolute', right: 10, top: 40 }} />}
          </View>

          <View style={{ borderBottomColor: '#B03C88', borderBottomWidth: 1, marginTop: 20, position: 'relative' }}>
            <Text style={{ fontSize: 14, color: '#fff', opacity: 0.7, fontFamily: 'Montserrat-Light', fontWeight: '700' }}>Password</Text>
            <TextInput secureTextEntry={true} style={{ color: '#fff', fontSize: 16, fontFamily: 'Montserrat-Light', fontWeight: '700' }}
              value={regObj.password} onChangeText={(text) => { setRegObj({ ...regObj, password: text }); checkValition() }} />
            {regObj.password != undefined && regObj.password.length >= 8 && <IonIcon name='checkmark-circle' size={16} color={'#fff'} style={{ position: 'absolute', right: 10, top: 40 }} />}
          </View>

          <View style={{ borderBottomColor: '#BE4F68', borderBottomWidth: 1, marginTop: 20, position: 'relative' }}>
            <Text style={{ fontSize: 14, color: '#fff', opacity: 0.7, fontFamily: 'Montserrat-Light', fontWeight: '700' }}>Confirm Password</Text>
            <TextInput secureTextEntry={true} style={{ color: '#fff', fontSize: 16, fontFamily: 'Montserrat-Light', fontWeight: '700' }}
              value={regObj.comfirmPassord} onChangeText={(text) => { setRegObj({ ...regObj, comfirmPassord: text }); checkValition() }} />
            {regObj.comfirmPassord != undefined && regObj.comfirmPassord.length >= 8 && <IonIcon name='checkmark-circle' size={16} color={'#fff'} style={{ position: 'absolute', right: 10, top: 40 }} />}
          </View>
        </View>

        <View style={{ flexDirection: 'row' }}>
          <Text style={{ color: '#fff', opacity: 0.7, fontFamily: 'Montserrat-Light', fontWeight: '700' }}>Already have an account?</Text>
          <TouchableOpacity onPress={() => { props.navigation.navigate('login') }}>
            <Text style={{ color: '#fff', fontWeight: '700', opacity: 0.9, fontFamily: 'Montserrat-Light', fontWeight: '700' }}>Login</Text>
          </TouchableOpacity>
        </View>

        <TouchableOpacity style={{ width: '100%', height: 60, marginTop: 20, borderRadius: 10 }}
          onPress={() => { register() }}>
          <LinearGradient
            colors={['#E98607', '#9013D1']} style={styles.loginLiner}>
            <Text style={{ color: '#fff', fontSize: 22, fontWeight: '700', fontFamily: 'Montserrat-SemiBold' }}>Register</Text>
          </LinearGradient>
        </TouchableOpacity>
      </ScrollView>
      {/* </LinearGradient> */}
    </View>
  );
};

export default Register;
