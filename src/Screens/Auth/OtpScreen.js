import { View, Text, Image, TouchableOpacity, TextInput } from 'react-native';
import React, { useEffect,useState } from 'react';
import LinearGradient from "react-native-linear-gradient";
import styles from './Styles';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import IonIcon from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import Constants from '../../../Helpers/constant';
import Spinner from '../../Styles/Spinner'
import Toaster from '../../Styles/Toaster'
import { checkForEmptyKeys, checkNumber, checkEmail } from '../../../serviceProvider/InputsNullChecker'

const OtpScreen = (props) => {

    const [loading, setLoading] = useState(false);
    const [filedCheck, setfiledCheck] = useState([]);
    const [regObj, setRegObj] = useState({
      name: '',
      email: '',
      phone: '',
      password: '',
      comfirmPassord: '',
      mpin:'',
      otp:'',

    });

    useEffect(()=>{
        getData();
    },[])

    const getData = async () =>{
        const userData = await AsyncStorage.getItem('regObj')
        console.log(userData)
        setRegObj(JSON.parse(userData))
    }
    const otpVerify = async () => {
        console.log(regObj)

          if(!checkNumber(regObj.otp) || regObj.otp.length < 4){
           return Toaster('invalid otp')
          }

          setLoading(true);
          try {
            axios.post(`${Constants.baseUrl}register`, regObj).then((res) => {
              setLoading(false);
              if (res && res.data?.status == 200) {
                
                AsyncStorage.removeItem('regObj');
                props.navigation.navigate('loginMpin');
              Toaster('Registered successfully')
              setRegObj({});
              }else{
                Toaster(res.data.phone[0]);
                Toaster(res.data.email[0]);
              }
           
              console.log(res.data)
            }).catch(err => {
              setLoading(false);
              console.log(err)
            })
          } catch (err) {
            console.log(err)
          }
        
    
      }

  return (
    <View style={{ flex: 1 }}>
        <Spinner color={'#fff'} visible={loading} />
            <LinearGradient
                colors={['#E98607', '#9013D1']} style={{ flex: 1 }}>
                <View style={styles.mainView}>
                    <Image source={require('../../Assets/Images/logo-new.png')} style={{ height: 100, width: 100 }} />

                    <View style={{ width: '100%', justifyContent: 'flex-start' }}>

                        {/* <Text style={{ fontSize: 25, color: '#fff', fontFamily: 'Montserrat-Light', fontWeight: '700', marginTop:50 }}>Forget Password</Text>
                        <Text style={{ fontSize: 16, color: '#fff', fontFamily: 'Montserrat-Light', fontWeight: '700',marginTop:20 }}>Confirm your email and we'll send the
                            instructions.</Text> */}

                        <View style={{ borderBottomColor: '#B03C88', borderBottomWidth: 1, marginTop: 30, position: 'relative' }}>
                            <Text style={{ fontSize: 14, color: '#fff', fontFamily: 'Montserrat-Light', fontWeight: '700' }}>OTP</Text>
                            <TextInput placeholder='Enter OTP' value={regObj.otp}  style={{ color: '#fff', fontSize: 18, fontFamily: 'Montserrat-SemiBold'}} onChangeText={(text)=>{setRegObj({...regObj, otp:text})}}/>
                           { regObj.otp != undefined && regObj.otp.length >= 4 && checkNumber(regObj.otp) && <IonIcon name='checkmark-circle' size={16} color={'#fff'} style={{ position: 'absolute', right: 10, top: 40 }} />}
                        </View>
                    </View>

                    <TouchableOpacity style={{ width: '100%', height: 60, marginTop: 20, borderRadius: 10 }}
                        onPress={() => { otpVerify() }}>
                        <LinearGradient
                            colors={['#E98607', '#9013D1']} style={styles.loginLiner}>
                            <Text style={{ color: '#fff', fontSize: 22, fontFamily: 'Montserrat-Light', fontWeight: '700' }}>OTP Verify</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
            </LinearGradient>
        </View>
  );
};

export default OtpScreen;
