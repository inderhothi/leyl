import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 30
  },
  container: {
    flex: 1,
    backgroundColor: '#000000',
    padding: 30
  },

  mainViewText: {
    color: '#fff',
    fontSize: 30,
    marginTop: 20,
    fontWeight: '700',
    fontFamily: 'Montserrat-Light'
  },

  mainViewText1: {
    color: '#fff',
    fontSize: 20,
    marginTop: 10,
    fontFamily: 'Montserrat-Light',
    fontWeight: '500'
  },

  loginLiner: {
    width: '100%',
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5
  },

  regbtn: {
    width: '100%',
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    borderColor: '#fff',
    borderWidth: 3,
    borderRadius: 10
  },

  regbtn2: {
    width: '100%',
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    borderWidth: 3,
    borderRadius: 10,
    backgroundColor: '#fff',
    marginTop: 100
  },
  // root: { padding: 20},
  title: { textAlign: 'left', fontSize: 18, color: '#fff' },
  codeFieldRoot2: { width: 250 },
  cell: {
    width: 70,
    height: 70,
    lineHeight: 68,
    fontSize: 24,
    borderWidth: 2,
    borderColor: '#fff',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    marginTop: 30,
    color: '#fff'
  },

  cell2: {
    width: 40,
    height: 40,
    lineHeight: 40,
    fontSize: 24,
    borderBottomWidth: 2,
    borderBottomColor: '#fff',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    marginTop: 10,
    color: '#fff'
  },
  focusCell: {
    borderColor: '#E98607',
  },

  changepasswordInput: {
    width: '100%', marginTop: 20, alignItems: 'center', borderBottomColor: 'grey', borderBottomWidth: 1, paddingBottom: 10
  }

});

export default styles;
