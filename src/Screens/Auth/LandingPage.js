import { View, Text, Image, TouchableOpacity } from 'react-native';
import React from 'react';
import LinearGradient from "react-native-linear-gradient";
import styles from './Styles';

const LandingPage = (props) => {
  return (
    <View style={{ flex: 1, backgroundColor: '#000000' }}>
      {/* <LinearGradient
        colors={['#E98607', '#9013D1']} style={{ flex: 1 }}> */}
      <View style={styles.mainView}>
        <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center' }}>
          <Image source={require('../../Assets/Images/logo-new.png')} style={{ height: 100, width: 100 }} />
          <Text style={styles.mainViewText}>Welcome</Text>
          <Text style={styles.mainViewText1}>Easiest way</Text>
          <Text style={{ color: '#fff', fontSize: 20, fontFamily: 'Montserrat-Light', fontWeight: '500' }}>Manage your money</Text>
        </View>

        <View style={{ flexDirection: 'row' }}>
          <View style={{ flex: 1, }}>
            <TouchableOpacity style={styles.regbtn2}
              onPress={() => { props.navigation.navigate('loginMpin') }}>
              <Text style={{ color: '#000000', fontSize: 18, fontWeight: '700', fontFamily: 'Montserrat-Light' }}>Login with MPin</Text>
            </TouchableOpacity>
          </View>
          <View style={{ flex: 1, }}>
            <TouchableOpacity style={styles.regbtn2}
              onPress={() => { props.navigation.navigate('login') }}>
              <Text style={{ color: '#000000', fontSize: 18, fontWeight: '700', fontFamily: 'Montserrat-Light' }}>Login with OTP</Text>
            </TouchableOpacity>
          </View>


        </View>

        {/* <TouchableOpacity style={styles.regbtn2}
          onPress={() => { props.navigation.navigate('loginMpin') }}>
          <Text style={{ color: '#000000', fontSize: 18, fontWeight: '700', fontFamily: 'Montserrat-Light' }}>Login</Text>
        </TouchableOpacity> */}

        <TouchableOpacity style={styles.regbtn}
          onPress={() => { props.navigation.navigate('stepOne') }}>
          <Text style={{ color: '#fff', fontSize: 20, fontWeight: '700', fontFamily: 'Montserrat-Light' }}>Register</Text>
        </TouchableOpacity>

        <TouchableOpacity style={{ marginTop: 25 }}>
          <Text style={{ color: '#fff', opacity: 0.7, fontFamily: 'Montserrat-Light', fontWeight: '700' }}> Terms of Service</Text>
        </TouchableOpacity>

      </View>
      {/* </LinearGradient> */}
    </View>
  );
};

export default LandingPage;

