import { View, Text, Image, TouchableOpacity, TextInput } from 'react-native';
import React, { useState } from 'react';
import LinearGradient from "react-native-linear-gradient";
import styles from './Styles';
import axios from 'axios';
import Constants from '../../../Helpers/constant';
import Spinner from '../../Styles/Spinner'
import Toaster from '../../Styles/Toaster'
import { checkNumber } from '../../../serviceProvider/InputsNullChecker'
import IonIcon from 'react-native-vector-icons/Ionicons';
import { Post } from '../../../Helpers/Service';


const CELL_COUNT = 6;
const ChangeMpin = (props) => {

    const [loading, setLoading] = useState(false);
    const [value, setvalue] = useState({
        old: null,
        new: null,
        confirm: null
    });

    const changeMpin = async () => {
        console.log(value)

        if (value.new.length != 6) {
            return Toaster('The new mpin should be fixed 6 digit')
        }

        if (value.old.length != 6) {
            return Toaster('The old mpin should be fixed 6 digit')
        }

        if (value.new != value.confirm) {
            return Toaster('Missmatch confirm Mpin')
        }

        try {
            setLoading(true);
            const data = {
                old_mpin: value.old,
                new_mpin: value.new,
            }
            console.log(data)
            Post('change-mpin', data).then(async (res) => {
                setLoading(false);
                console.log(res.data)
                if (res && res.status == 200) {
                    props.navigation.replace('loginMpin');
                    Toaster(res.message);
                } else {

                }
                console.log(res.data)
            }).catch(err => {
                setLoading(false);
                console.log(err)
            })
        } catch (err) {
            setLoading(false);
            console.log(err)
        }
    }


    return (
        <View style={{ flex: 1, backgroundColor: '#000000' }}>
            <Spinner color={'#fff'} visible={loading} />

            <View style={styles.mainView}>
                <Image source={require('../../Assets/Images/logo-new.png')} style={{ height: 100, width: 100 }} />

                <View style={{ borderBottomColor: '#BE4F68', borderBottomWidth: 1, marginTop: 20, position: 'relative', width: '100%' }}>
                    <Text style={{ fontSize: 14, color: '#fff', fontFamily: 'Montserrat-Light', fontWeight: '700' }}>Old Mpin</Text>
                    <TextInput secureTextEntry={true} style={{ color: '#fff', fontSize: 18, fontFamily: 'Montserrat-Light', fontWeight: '700' }}
                        value={value.old} onChangeText={(text) => { setvalue({ ...value, old: text }) }} keyboardType='numeric' />
                    {value.old != undefined && value.old.length == 6 && <IonIcon name='checkmark-circle' size={16} color={'#fff'} style={{ position: 'absolute', right: 10, top: 40 }} />}
                </View>

                <View style={{ borderBottomColor: '#BE4F68', borderBottomWidth: 1, marginTop: 20, position: 'relative', width: '100%' }}>
                    <Text style={{ fontSize: 14, color: '#fff', fontFamily: 'Montserrat-Light', fontWeight: '700' }}>New Mpin</Text>
                    <TextInput secureTextEntry={true} style={{ color: '#fff', fontSize: 18, fontFamily: 'Montserrat-Light', fontWeight: '700' }}
                        value={value.new} onChangeText={(text) => { setvalue({ ...value, new: text }) }} keyboardType='numeric' />
                    {value.new != undefined && value.new.length == 6 && <IonIcon name='checkmark-circle' size={16} color={'#fff'} style={{ position: 'absolute', right: 10, top: 40 }} />}
                </View>

                <View style={{ borderBottomColor: '#BE4F68', borderBottomWidth: 1, marginTop: 20, position: 'relative', width: '100%' }}>
                    <Text style={{ fontSize: 14, color: '#fff', fontFamily: 'Montserrat-Light', fontWeight: '700' }}>Confirm Mpin</Text>
                    <TextInput secureTextEntry={true} style={{ color: '#fff', fontSize: 18, fontFamily: 'Montserrat-Light', fontWeight: '700' }}
                        value={value.confirm} onChangeText={(text) => { setvalue({ ...value, confirm: text }) }} keyboardType='numeric' />
                    {value.confirm != undefined && value.confirm.length == 0 && <IonIcon name='checkmark-circle' size={16} color={'#fff'} style={{ position: 'absolute', right: 10, top: 40 }} />}
                </View>

                <TouchableOpacity style={{ width: '100%', height: 60, marginTop: 50, borderRadius: 10 }}
                    onPress={() => {
                        changeMpin();
                        // loginfaceId()
                    }}>
                    <LinearGradient
                        colors={['#E98607', '#9013D1']} style={styles.loginLiner}>
                        <Text style={{ color: '#fff', fontSize: 22, fontFamily: 'Montserrat-Light', fontWeight: '700' }}>Change MPIN</Text>
                    </LinearGradient>
                </TouchableOpacity>
                <View style={{ flexDirection: 'row', marginTop: 20 }}>
                    <TouchableOpacity onPress={() => { props.navigation.navigate('loginMpin') }}>
                        <Text style={{ color: '#fff', fontFamily: 'Montserrat-Bold', fontWeight: '700', fontSize: 16 }}>Login</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

export default ChangeMpin