import { View, Text, Image, TouchableOpacity, TextInput } from 'react-native';
import React, { useEffect, useState } from 'react';
import LinearGradient from "react-native-linear-gradient";
import styles from './Styles';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import IonIcon from 'react-native-vector-icons/Ionicons';
import axios from 'axios';
import Constants from '../../../Helpers/constant';
import Spinner from '../../Styles/Spinner'
import Toaster from '../../Styles/Toaster'
import { checkForEmptyKeys, checkNumber, checkEmail } from '../../../serviceProvider/InputsNullChecker'

const Forgotpassword = (props) => {
    const [email, setEmail] = useState('johndeo@gmail.com')

    // const sendOtp = async () => {

    //     if(!checkNumber(phone) || phone.length < 10){
    //       return Toaster('The phone number does not valid.')
    //     }
    //     setLoading(true);
    //     try {
    //       axios.post(`${Constants.baseUrl}forget-password`, {phone}).then((res) => {
    //         setLoading(false);
    //         if (res && res.data.status == 200) {

    //         props.navigation.navigate('login',{phone});
    //         Toaster('Otp sent successfully')
    //         setRegObj({});

    //         }else{
    //           Toaster(res.data.message);
    //         }

    //         console.log(res.data)
    //       }).catch(err => {
    //         setLoading(false);
    //         console.log(err)
    //       })
    //     } catch (err) {
    //       console.log(err)
    //     }


    // }
    return (
        <View style={{ flex: 1, backgroundColor: '#000000' }}>
            {/* <LinearGradient
                colors={['#E98607', '#9013D1']} style={{ flex: 1 }}> */}
            <View style={styles.mainView}>
                <Image source={require('../../Assets/Images/logo-new.png')} style={{ height: 100, width: 100 }} />

                <View style={{ width: '100%', justifyContent: 'flex-start' }}>

                    <Text style={{ fontSize: 25, color: '#fff', fontFamily: 'Montserrat-Light', fontWeight: '700', marginTop: 50 }}>Forget Password</Text>
                    <Text style={{ fontSize: 16, color: '#fff', fontFamily: 'Montserrat-Light', fontWeight: '700', marginTop: 20 }}>Confirm your email and we'll send the
                        instructions.</Text>

                    <View style={{ borderBottomColor: '#B03C88', borderBottomWidth: 1, marginTop: 30, position: 'relative' }}>
                        <Text style={{ fontSize: 14, color: '#fff', fontFamily: 'Montserrat-Light', fontWeight: '700' }}>Email</Text>
                        <TextInput value={email} style={{ color: '#fff', fontSize: 18, fontFamily: 'Montserrat-SemiBold' }} />
                        {email != undefined && email.length > 0 && checkEmail(email) && <IonIcon name='checkmark-circle' size={16} color={'#fff'} style={{ position: 'absolute', right: 10, top: 40 }} />}
                    </View>
                </View>

                <TouchableOpacity style={{ width: '100%', height: 60, marginTop: 20, borderRadius: 10 }}
                    onPress={() => { props.navigation.navigate('login') }}>
                    <LinearGradient
                        colors={['#E98607', '#9013D1']} style={styles.loginLiner}>
                        <Text style={{ color: '#fff', fontSize: 22, fontFamily: 'Montserrat-Light', fontWeight: '700' }}>Reset Password</Text>
                    </LinearGradient>
                </TouchableOpacity>
            </View>
            {/* </LinearGradient> */}
        </View>
    );
};

export default Forgotpassword;
