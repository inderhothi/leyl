import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import React, { useEffect, useState } from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Login from '../Screens/Auth/Login';
import Register from '../Screens/Auth/Register';
import LandingPage from '../Screens/Auth/LandingPage';
import Home from '../Screens/MainRoute/Tabs/Home';
import Scan from '../Screens/MainRoute/Tabs/Scan';
import Order from '../Screens/MainRoute/Tabs/Order';
import Account from '../Screens/MainRoute/Tabs/Account';
import Forgotpassword from '../Screens/Auth/Forgotpassword';
import AddProduct from '../Screens/MainRoute/Tabs/AddProduct';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import Transactions from '../Screens/MainRoute/Transaction/Transactions';
import OtpScreen from '../Screens/Auth/OtpScreen';
import MyQRcode from '../Screens/MainRoute/MyQRcode';
import SendMoney from '../Screens/MainRoute/SendMoney';
import LoginOtp from '../Screens/Auth/LoginOtp';
import StepOne from '../Screens/Auth/ResiterFlow/StepOne';
import StepTwo from '../Screens/Auth/ResiterFlow/StepTwo';
import StepThree from '../Screens/Auth/ResiterFlow/StepThree';
import StepFour from '../Screens/Auth/ResiterFlow/StepFour';
import StepFive from '../Screens/Auth/ResiterFlow/StepFive';
import StepSix from '../Screens/Auth/ResiterFlow/StepSix';
import StepSeven from '../Screens/Auth/ResiterFlow/StepSeven';
import StepEight from '../Screens/Auth/ResiterFlow/StepEight';
import SplashScreen from '../Screens/Auth/SplashScreen'
import LoginwithMpin from '../Screens/Auth/LoginwithMpin';
import RequestMoney from '../Screens/MainRoute/Money/RequestMoney';
import SplitMoney from '../Screens/MainRoute/Money/SplitMoney';
import CheckBalance from '../Screens/MainRoute/Money/CheckBalance';
import { Avatar } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';
import TransactionById from '../Screens/MainRoute/Transaction/TransactionById';
import OtpVerify from '../Screens/Auth/ResiterFlow/OtpVerify';
import ChangeMpin from '../Screens/Auth/ChangeMpin';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();
const topTab = createMaterialTopTabNavigator();

const MainRoute = () => {
    const [userDetail, setUserDetail] = useState({})

    let navigation = useNavigation();
    useEffect(() => {
        {
            async () => {
                const user = await AsyncStorage.getItem('userDetail')
                setUserDetail(JSON.stringify(user))
            }
        }
    }, [])
    console.log("user====================>", userDetail)

    const option = (title) => {
        const opt = {
            title,
            headerStyle: {
                backgroundColor: '#000000',
                height: 40
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontFamily: 'Montserrat-SemiBold',
                fontSize: 16
            },
            headerRight: () => (
                <TouchableOpacity onPress={() => { navigation.navigate('Account') }}>
                    <Avatar.Image size={24} source={require('../Assets/Images/faceid.png')} />
                </TouchableOpacity>
            ),
        }
        return opt;
    }

    const option2 = (title) => {
        const opt = {
            title,
            headerStyle: {
                backgroundColor: '#000000',
                height: 40
            },
            headerTitle: (props) => <LogoTitle />,
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontFamily: 'Montserrat-SemiBold',
                fontSize: 16
            },
            headerBackVisible: false,
            headerRight: () => (
                <TouchableOpacity onPress={() => { navigation.navigate('Account') }}>
                    <Avatar.Image size={24} source={require('../Assets/Images/faceid.png')} />
                </TouchableOpacity>
            ),
        }
        return opt;
    }

    function LogoTitle() {
        return (
            <View style={{ width: '100%', flexDirection: 'row', }}>
                <Image
                    resizeMode="contain"
                    source={require('../Assets/Images/header-logo.png')}
                    style={{ height: 30, width: 100 }}
                />
                {/* <Text style={{color:'#fff',fontSize:20,marginLeft:20,fontFamily:'Montserrat-SemiBold'}}>Leyl</Text> */}
            </View>
        );
    }


    const Taboption = (title) => {
        const opt = {
            headerTitle: title,
            headerStyle: {
                backgroundColor: '#9519C8',
                height: 40
            },
            headerTintColor: '#fff',

            headerTitleStyle: {
                fontFamily: 'Montserrat-SemiBold',
                fontSize: 16
            },
        }
        return opt;
    }

    return (


        // <>
        //     {userDetail != {} &&
        <Stack.Navigator>
            <Stack.Screen options={{ headerShown: false }} name="splash" component={SplashScreen} />
            <Stack.Screen options={{ headerShown: false }} name="landingpage" component={LandingPage} />
            <Stack.Screen options={{ headerShown: false }} name="login" component={Login} />
            <Stack.Screen options={{ headerShown: false }} name="register" component={Register} />
            <Stack.Screen options={{ headerShown: false }} name="forgotpassword" component={Forgotpassword} />
            <Stack.Screen options={{ headerShown: false }} name="otpScreen" component={OtpScreen} />
            <Stack.Screen options={{ headerShown: false }} name="loginOtp" component={LoginOtp} />
            <Stack.Screen options={{ headerShown: false }} name="loginMpin" component={LoginwithMpin} />
            <Stack.Screen options={{ headerShown: false }} name="changeMpin" component={ChangeMpin} />

            <Stack.Screen options={{ headerShown: false }} name="stepOne" component={StepOne} />
            <Stack.Screen options={{ headerShown: false }} name="stepTwo" component={StepTwo} />
            <Stack.Screen options={{ headerShown: false }} name="otpVerify" component={OtpVerify} />
            <Stack.Screen options={{ headerShown: false }} name="stepThree" component={StepThree} />
            <Stack.Screen options={{ headerShown: false }} name="stepFour" component={StepFour} />
            <Stack.Screen options={{ headerShown: false }} name="stepFive" component={StepFive} />
            <Stack.Screen options={{ headerShown: false }} name="stepSix" component={StepSix} />
            <Stack.Screen options={{ headerShown: false }} name="stepSeven" component={StepSeven} />
            <Stack.Screen options={{ headerShown: false }} name="stepEight" component={StepEight} />
            {/* </Stack.Navigator>
            } */}

            {/* {userDetail == {} &&
                <Stack.Navigator> */}
            <Stack.Screen name="Home" component={Home} options={option2('Leyl')} />
            <Stack.Screen name="Scan" component={Scan} options={option('Scan')} />
            <Stack.Screen name="Add" component={AddProduct} />
            <Stack.Screen name="Order" component={Order} />
            <Stack.Screen name="Account" component={Account} />


            <Stack.Screen options={{ headerShown: false }} name="mainRoute" >
                {
                    () => (
                        <Tab.Navigator
                            screenOptions={({ route }) => ({
                                tabBarActiveTintColor: 'grey',
                                tabBarInactiveTintColor: 'lightgrey',
                                tabBarIcon: ({ focused, size, color }) => {
                                    let iconName;
                                    if (route.name === 'Home') {
                                        iconName = 'home';
                                        size = focused ? 20 : 20;
                                        color = focused ? 'grey' : 'lightgrey';
                                    } else if (route.name === 'Scan') {
                                        iconName = 'qrcode';
                                        size = focused ? 20 : 20;
                                        color = focused ? 'grey' : 'lightgrey';
                                    } else if (route.name === 'Add') {

                                    } else if (route.name === 'Order') {
                                        iconName = 'dollar-sign';
                                        size = focused ? 20 : 20;
                                        color = focused ? 'grey' : 'lightgrey';
                                    } else if (route.name === 'Account') {
                                        iconName = 'user';
                                        size = focused ? 20 : 20;
                                        color = focused ? 'grey' : 'lightgrey';
                                    }
                                    return (
                                        <>
                                            {route.name == 'Add' ?
                                                <Image source={require('../Assets/Images/Plus.png')}
                                                    style={{ height: 100, width: 100, position: 'absolute', top: -50 }}
                                                />
                                                :
                                                <FontAwesome5
                                                    name={iconName}
                                                    size={size}
                                                    color={color}
                                                />
                                            }
                                        </>
                                    )
                                },
                                tabBarStyle: {
                                    height: 70,
                                    ...styles.shadow,
                                    paddingTop: 10
                                },
                                tabBarLabelStyle: {
                                    fontSize: 12,
                                    fontFamily: 'Montserrat-SemiBold',
                                    paddingBottom: 10,
                                    display: route.name == 'Add' ? 'none' : 'flex'
                                }
                            })
                            }
                        >
                            <Tab.Screen name="Home" component={Home} options={Taboption('Leyl')} />
                            <Tab.Screen name="Scan" component={Scan} options={Taboption('Scan')} />
                            <Tab.Screen name="Add" component={AddProduct} />
                            <Tab.Screen name="Order" component={Order} />
                            <Tab.Screen name="Account" component={Account} />
                        </Tab.Navigator>
                    )
                }
            </Stack.Screen>
            <Stack.Screen name="transactions" component={Transactions} options={option('Transactions')} />
            <Stack.Screen name="transactionbyId" component={TransactionById} options={option('Transactions')} />
            <Stack.Screen name="myQR" component={MyQRcode} options={option('My QR Code')} />
            <Stack.Screen name="sendMoney" component={SendMoney} options={option('Send')} />

            <Stack.Screen name="checkBalance" component={CheckBalance} options={option('Account Balance')} />

            <Stack.Screen name="Money" options={option('Request and Split')} >
                {
                    () => (
                        <topTab.Navigator
                            screenOptions={{
                                tabBarActiveTintColor: '#fff',
                                tabBarLabelStyle: { fontSize: 16, textTransform: 'none', },
                                tabBarStyle: { backgroundColor: '#1D1D1D', },
                                tabBarIndicatorStyle: { backgroundColor: '#C70237' },
                                swipeEnabled: false
                            }}>
                            <topTab.Screen name="requestMoney" component={RequestMoney} options={{ tabBarLabel: 'Request' }} />
                            <topTab.Screen name="splitMoney" component={SplitMoney} options={{ tabBarLabel: 'Split Amount' }} />
                        </topTab.Navigator>
                    )
                }
            </Stack.Screen>
        </Stack.Navigator>
        // }
        // </>
    );
};

const styles = StyleSheet.create({
    shadow: {
        shadowColor: '#F4F4F4',
        shadowOffset: {
            width: 0,
            height: 10
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.5,
        elevation: 5
    }
});

export default MainRoute;
