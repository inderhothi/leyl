import axios from 'axios';
import Constants from './constant';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Post = async (url, data) => {
    const user = await AsyncStorage.getItem('userDetail');
    let userDetail = JSON.parse(user)
    return axios.post(Constants.baseUrl + url, data, {
        headers: {
            'Authorization': `Bearer ${userDetail.token}`
        },
    }).then((res) => {
        console.log(res.data)
        return res.data
    }).catch(err => {
        console.log(err)
        return err
    })

}

const GetApi = async (url) => {
    const user = await AsyncStorage.getItem('userDetail');
    let userDetail = JSON.parse(user)
    return axios.get(Constants.baseUrl + url, {
        headers: {
            'Authorization': `Bearer ${userDetail.token}`
        },
    }).then((res) => {
        console.log(res.data)
        return res.data
    }).catch(err => {
        console.log(err)
        return err
    })

}

export { Post, GetApi }
